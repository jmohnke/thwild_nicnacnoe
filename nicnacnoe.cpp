#include <fstream>
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <iostream>
#include <alcommon/albroker.h>
#include <alproxies/alphotocaptureproxy.h>
#include <alproxies/alsystemproxy.h>
#include <alvision/alvisiondefinitions.h>
#include <alvision/alimage.h>
#include <boost/algorithm/string.hpp>
#include "nicnacnoe.h"
#include "draw.h"

/**
 * @brief Konstruktor des Moduls.
 *
 * @param broker Der verwendete Broker aus der naoqi.
 * @param name Der Name des Moduls, unter welchem es ansprechbar ist.
 */
Nicnacnoe::Nicnacnoe(boost::shared_ptr<ALBroker> broker, const std::string &name):
    ALModule(broker,name),
    memoryProxy(getParentBroker()),
    videoDeviceProxy(getParentBroker()),
    textToSpeechProxy(getParentBroker()),
    speechRecognitionProxy(getParentBroker()),
    autonomousMovesProxy(getParentBroker()) {

    functionName("start", getName(), "Spiele eine Runde TicTacToe auf naoisch!");
        //addParam("handName", "linke-LHand- oder rechte-RHand- Hand");
        BIND_METHOD(Nicnacnoe::start);
    functionName("stop", getName(), "Beendet die Spielrunde");
        BIND_METHOD(Nicnacnoe::stop);
    functionName("onGetPen", getName(), "Aktionen sobald der Stift in der Hand ist!");
        BIND_METHOD(Nicnacnoe::onGetPen);
    functionName("onGetMovement", getName(), "Der nao erhält den Befehl seinen Zug zu tätigen.");
        BIND_METHOD(Nicnacnoe::onGetMovement);
    functionName("onBumperPressed", getName(), "Aktion wenn right bumper gedrückt wird.");
        BIND_METHOD(Nicnacnoe::onBumperPressed);
    functionName("onWordRecognized", getName(), "Aktion sobald ein Word aus dem Vokabular erkannt wird.");
        BIND_METHOD(Nicnacnoe::onWordRecognized);
    functionName("onCalibrateGameBoard", getName(), "Richtet das Spielfeld in der Realität aus.");
        BIND_METHOD(Nicnacnoe::onCalibrateGameBoard);

    draw = boost::shared_ptr<ALProxy>(new ALProxy(broker, "Draw"));
    boardCamera = videoDeviceProxy.subscribeCamera("nicnnacnoeboardcam", 1, kVGA, khsYColorSpace, 1);
}

/**
 * @brief Destruktor des Moduls.
 */
Nicnacnoe::~Nicnacnoe(){
    videoDeviceProxy.releaseImage(boardCamera);
    stop();
}

/**
 * @brief Überschreibt die Init, welche zum Start des naoqi automatisch aufgerufen wird.
 */
void Nicnacnoe::init() {
    //Create vocabulary for the recognition and set it to the proxy
    ALSystemProxy systemProxy(getParentBroker());
    robotname = systemProxy.robotName();
    speechRecognitionProxy.pause(true);
    std::string arr[] = {robotname,"eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun", "nochmal"};
    std::vector<std::string> voc(arr, arr+11);
    speechRecognitionProxy.setWordListAsVocabulary(voc);
    speechRecognitionProxy.setVisualExpression(false); //Disable the visual expression when detectic words
    speechRecognitionProxy.setLanguage("German"); //Set the language to german
    textToSpeechProxy.say("Ich kann spielen, rufe dafür meinen Namen!");
    stateWaitToStart = true;
    memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
    autonomousMovesProxy.setExpressiveListeningEnabled(false);
    speechRecognitionProxy.pause(false);
}

/**
 * @brief Startet das Modul und initialisert eine neue Spielrunde.
 */
void Nicnacnoe::start(){
    textToSpeechProxy.say("Lass uns Tictactoe spielen.");
    textToSpeechProxy.say("Es wäre nett wen du mich direkt vor den Tisch stellen könntest und mir einen Stift reichen kannst.");
    memoryProxy.subscribeToEvent("PenInHand", getName(), "onGetPen");
    memoryProxy.subscribeToEvent("FrontTactilTouched", getName(), "calibrateGameBoard");
    memoryProxy.subscribeToEvent("RightBumperPressed", getName(), "onBumperPressed");
    // Einlegeposition einnehmen
    draw->callVoid("grapPen");
    // initialisiere Spielfeld
    for (int i = 0; i<9; i++){
        currentGameboard[i] = FREE;
    }
}

/**
 * @brief Beendet das laufende Spiel und koppel sich von allen Events ab.
 */
void Nicnacnoe::stop(){
    std::cout << "ENDGAME";
    memoryProxy.unsubscribeToEvent("RightBumperPressed", getName());
    memoryProxy.unsubscribeToEvent("PenInHand", getName());
    memoryProxy.unsubscribeToEvent("MiddleTactilTouched", getName());
    memoryProxy.unsubscribeToEvent("FrontTactilTouched", getName());
    //Ausgangsstatus einnehmen
    stateWaitToStart = true;
    draw->callVoid("losePen");
    memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
}

/**
 * @brief Wird aufgerufen sobald der Roboter eine Veränderung am Stift feststellt.
 */
void Nicnacnoe::onGetPen(){
    if (memoryProxy.getData("hand")){
        // hand ist geschlossen (Stift in Hand)
        textToSpeechProxy.say("Mit dem Stift in der Hand werde ich wohl nur gewinnen können!");
        textToSpeechProxy.say("Da ich noch nicht richtig sehen kann, must du mir anders Bescheid geben, dass ich an der Reihe bin. Berühe mein Kopfsensor wenn ich an der Reihe bin!");
        textToSpeechProxy.say("Na dann los - Mache eine dicke Markierung oder berürhe mein Kopfsensor!");
        textToSpeechProxy.say("Wenn du das Spiel vorzeitig beendet möchtest, hier noch ein Tip. Ohne Stift kann ich nicht mit dir spielen.");
        memoryProxy.subscribeToEvent("MiddleTactilTouched", getName(), "onGetMovement");
    }else{
        // hand ist offen (kein Stift)
        textToSpeechProxy.say("Ich hätte unermüdlich weitergemacht!");
        stop();
    }
}

/**
 * @brief Wird aufgerufen, wenn der Tisch sich ändert.
 */
void Nicnacnoe::onBumperPressed(){
    float table = memoryProxy.getData("RightBumperPressed");
    if(table){
        textToSpeechProxy.say("Danke für den Tisch!");
    }else{
        textToSpeechProxy.say("Ich kann den Tisch nicht mehr sehen!");
    }
}

/**
 * @brief Nao kommt an der Reihe und führt seinen Spielzug aus.
 */
void Nicnacnoe::onGetMovement(){
    float table = memoryProxy.getData("RightBumperPressed");
    float touched = memoryProxy.getData("MiddleTactilTouched");

    // Prüfe den Tisch
    if ((touched == 1.0f || forceDoMovement) && table == 0.0f){
        textToSpeechProxy.say("Ich kann den Tisch noch nicht sehen! Schiebe mich einfach an meine Kiste heran!");
    }
    // Prüfe den Stift
    if (!memoryProxy.getData("hand")){
        textToSpeechProxy.say("Das ist unfair, ich habe keinen Stift!");
    }
    //spiele den Zug wenn alle Voraussetzung gegeben sind
    if ((touched == 1.0f || forceDoMovement) && table == 1.0f){
        //suche Feld vom User
        int newField = findNewField();
        if (newField != -1){
            //neues Feld wurde gefunden und muss somit dem USER gehören
            currentGameboard[newField] = USER;
            doMovement();
        }else if (boardEmpty()){
            // nix erkannt und das Spielfeld ist leer == NAO soll beginnen
            textToSpeechProxy.say("Na dann fange ich mal an.");
            doMovement();
        }else if (!boardEmpty()){
            // nix erkannt, aber es sind schon Züge vorhanden
            textToSpeechProxy.say("Hast du überhaupt deinen Zug getätigt?");
            textToSpeechProxy.say("Welches Feld hast du denn markiert? Oder soll ich nochmal genau schauen?");
            memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
            autonomousMovesProxy.setExpressiveListeningEnabled(false);
        }else {

        }
    }
    forceDoMovement = false;
}

/**
 * @brief Wird aufgerufen wenn ein Word erkannt wird.
 *
 * @param eventName Name of the event [WordRecognized]
 * @param value Holds the value which word is recognized [index 0] and the confidence of the recognition[index 1]
 * @param subscriberIdentifier The name of the subscriber of the event
 */
void Nicnacnoe::onWordRecognized(const std::string &eventName, const AL::ALValue &value, const std::string &subscriberIdentifier){
    //Eventbindung aufheben
    autonomousMovesProxy.setExpressiveListeningEnabled(false);
    memoryProxy.unsubscribeToEvent("WordRecognized", getName());

    int newField = -1;
    //Get the recognized wird and the confidence
    std::string recognizedWord = value[0].toString();
    boost::algorithm::replace_all(recognizedWord, "\"", "");
    float confidence = (float) value[1];

    //erkenne Userfeld robotname, "eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun", "nochmal"
    if( stateWaitToStart && recognizedWord == robotname && confidence >= 0.4){
        stateWaitToStart = false;
        start();
    }else if (!stateWaitToStart){
        if (recognizedWord == "nochmal" && confidence >= 0.4){
            textToSpeechProxy.say("Na dann schaue ich noch einmal genau hin!");
            forceDoMovement = true;
            onGetMovement();
        }else if (recognizedWord == "eins" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die eins.");
            newField = 0;
        }else if (recognizedWord == "zwei" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die zwei.");
            newField = 1;
        }else if (recognizedWord == "drei" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die drei.");
            newField = 2;
        }else if (recognizedWord == "vier" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die vier.");
            newField = 3;
        }else if (recognizedWord == "fünf" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die fünf.");
            newField = 4;
        }else if (recognizedWord == "sechs" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die sechs.");
            newField = 5;
        }else if (recognizedWord == "sieben" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die sieben.");
            newField = 6;
        }else if (recognizedWord == "acht" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die acht.");
            newField = 7;
        }else if (recognizedWord == "neun" && confidence >= 0.4){
            textToSpeechProxy.say("Ahh, die neun.");
            newField = 8;
        }else {
            textToSpeechProxy.say("Wie bitte!");
            autonomousMovesProxy.setExpressiveListeningEnabled(false);
            memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
        }
        if (newField != -1){
            if (currentGameboard[newField] == FREE){
            currentGameboard[newField] = USER;
            doMovement();
            }else{
                textToSpeechProxy.say("Hey, du schummelst. Da war schon etwas!");
                textToSpeechProxy.say("Welches Feld hast du denn nun wirklich markiert? Oder soll ich nochmal genau schauen?");
                memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
            }
        }
    }else{
        autonomousMovesProxy.setExpressiveListeningEnabled(false);
        memoryProxy.subscribeToEvent("WordRecognized", getName(), "onWordRecognized");
    }
}

/**
 * @brief Gibt dem Benutzer eine Hilfe um das leere Spielfeld auszurichten.
 */
void Nicnacnoe::onCalibrateGameBoard(){
    float touched = memoryProxy.getData("FrontTactilTouched");
    if (touched == 1.0f){
        textToSpeechProxy.say("Richte das leere Spielfeld aus.");
        textToSpeechProxy.say("Ich schaue!");
        if (findNewField() != -1){
            textToSpeechProxy.say("Das Feld liegt falsch!");
        }else{
            textToSpeechProxy.say("Super, das sollte passen!");
        }
    }
}

/**
 * @brief Sucht ein neu hinzugekommenes Feld, welches nicht vom nao gekennzeichnet wurde.
 *
 * @return int Die Feldnummer des gefundenen Feldes.
 */
int Nicnacnoe::findNewField(){
    int newField = -1;

    //todo analysiere Spielfeld
    motionInitToScan();
    videoDeviceProxy.setActiveCamera(1);
#ifndef NICNACNOE_IS_REMOTE
        ALImage *image;
        image = ( ALImage* ) videoDeviceProxy.getImageLocal(boardCamera);
        int width = image->getWidth();
        int height = image->getHeight();
        const unsigned char* dataPointer = image->getData();
#endif

#ifdef NICNACNOE_IS_REMOTE
    ALValue image;
    image.arraySetSize(12);
    image = videoDeviceProxy.getImageRemote(boardCamera);
    int width = (int) image[0];
    int height = (int) image[1];
    const unsigned char* dataPointer =  static_cast<const unsigned char*>(image[6].GetBinary());
#endif
    //finde erstes neues Feld
    for (int field = 0; field < 9; field++) {
        if (currentGameboard[field] == FREE && getFieldStatus(dataPointer, width, height, field) ){
            std::cout << "erkanntes Feld:" << field+1 << std::endl;
            newField = field;
            break;
        }
    }
    // Release image
    videoDeviceProxy.releaseImage(boardCamera);
    motionScanToInit();
    return newField;
}

/**
 * @brief Führt einen Spielzug aus.
 */
void Nicnacnoe::doMovement(){
    float table = memoryProxy.getData("RightBumperPressed");

    // Prüfe den Tisch
    if (table == 0.0f){
        textToSpeechProxy.say("Ich kann den Tisch noch nicht sehen! Schiebe mich einfach an meine Kiste heran!");
    }
    // Prüfe den Stift
    if (!memoryProxy.getData("hand")){
        textToSpeechProxy.say("Das ist unfair, ich habe keinen Stift!");
    }
    //spiele den Zug wenn alle Voraussetzung gegeben sind
    if (table == 1.0f && memoryProxy.getData("hand")){
        int win = checkWinner();
        //bestimme ob user bereits gewonnen hat
        switch (win){
        case USER:
            textToSpeechProxy.say("Da war wohl jemand besser als ich? Glückwunsch.");
            break;
        case NAO:
            textToSpeechProxy.say("Ich habe gewonnen!");
            break;
        case DRAW:
            textToSpeechProxy.say("Haa! Unentschieden.");
            break;
        default:
            //such ein freies Feld aus
            int newField = chooseFreeField();
            //mache eine Markierung in newField
            std::cout << "markiere " << newField+1 << std::endl;
            currentGameboard[newField] = NAO;
            draw->callVoid("drawField",newField);
            win = checkWinner();
            if (win == NAO) textToSpeechProxy.say("Ich habe gewonnen!");
        }
        printCurrentGameBoard();
        if (win != 0){
            textToSpeechProxy.say("Komm, gleich noch eine Runde!");
            // initialisiere Spielfeld
            for (int i = 0; i<9; i++){
                currentGameboard[i] = FREE;
            }
        }
    }
}

/**
 * @brief Gibt ein leeres Feld für einen sinnvollen Spielzug zurück.
 *
 * @return int Die Feldnummer, welche gemarkert werden sollte.
 */
int Nicnacnoe::chooseFreeField(){
    // felder auf den NAO gewinnen würde
    for (int i=0; i<9; i++){
        if (currentGameboard[i]==FREE){
            if(       i == 0 && ((currentGameboard[1] == currentGameboard[2] && currentGameboard[2] == NAO) ||
                                 (currentGameboard[3] == currentGameboard[6] && currentGameboard[6] == NAO) ||
                                 (currentGameboard[4] == currentGameboard[8] && currentGameboard[8] == NAO) )){
                      return 0;
            }else if( i == 1 && ((currentGameboard[0] == currentGameboard[2] && currentGameboard[2] == NAO) ||
                                 (currentGameboard[4] == currentGameboard[7] && currentGameboard[7] == NAO) )){
                      return 1;
            }else if( i == 2 && ((currentGameboard[0] == currentGameboard[1] && currentGameboard[1] == NAO) ||
                                 (currentGameboard[5] == currentGameboard[8] && currentGameboard[8] == NAO) ||
                                 (currentGameboard[4] == currentGameboard[6] && currentGameboard[6] == NAO) )){
                      return 2;
            }else if( i == 3 && ((currentGameboard[4] == currentGameboard[5] && currentGameboard[5] == NAO) ||
                                 (currentGameboard[0] == currentGameboard[6] && currentGameboard[6] == NAO) )){
                      return 3;
            }else if( i == 4 && ((currentGameboard[3] == currentGameboard[5] && currentGameboard[5] == NAO) ||
                                 (currentGameboard[1] == currentGameboard[7] && currentGameboard[7] == NAO) ||
                                 (currentGameboard[0] == currentGameboard[8] && currentGameboard[8] == NAO) ||
                                 (currentGameboard[2] == currentGameboard[6] && currentGameboard[6] == NAO) )){
                      return 4;
            }else if( i == 5 && ((currentGameboard[3] == currentGameboard[4] && currentGameboard[4] == NAO) ||
                                 (currentGameboard[2] == currentGameboard[8] && currentGameboard[8] == NAO) )){
                      return 5;
            }else if( i == 6 && ((currentGameboard[7] == currentGameboard[8] && currentGameboard[8] == NAO) ||
                                 (currentGameboard[0] == currentGameboard[3] && currentGameboard[3] == NAO) ||
                                 (currentGameboard[2] == currentGameboard[4] && currentGameboard[4] == NAO) )){
                      return 6;
            }else if( i == 7 && ((currentGameboard[6] == currentGameboard[8] && currentGameboard[8] == NAO) ||
                                 (currentGameboard[1] == currentGameboard[4] && currentGameboard[4] == NAO) )){
                      return 7;
            }else if( i == 8 && ((currentGameboard[6] == currentGameboard[7] && currentGameboard[7] == NAO) ||
                                 (currentGameboard[2] == currentGameboard[5] && currentGameboard[5] == NAO) ||
                                 (currentGameboard[0] == currentGameboard[4] && currentGameboard[4] == NAO) )){
                      return 8;
            }
        }
    }
    // felder auf den USER gewinnen würde
    for (int i=0; i<9; i++){
        if (currentGameboard[i]==FREE){
            if(       i == 0 && ((currentGameboard[1] == currentGameboard[2] && currentGameboard[2] == USER) ||
                                 (currentGameboard[3] == currentGameboard[6] && currentGameboard[6] == USER) ||
                                 (currentGameboard[4] == currentGameboard[8] && currentGameboard[8] == USER) )){
                      return 0;
            }else if( i == 1 && ((currentGameboard[0] == currentGameboard[2] && currentGameboard[2] == USER) ||
                                 (currentGameboard[4] == currentGameboard[7] && currentGameboard[7] == USER) )){
                      return 1;
            }else if( i == 2 && ((currentGameboard[0] == currentGameboard[1] && currentGameboard[1] == USER) ||
                                 (currentGameboard[5] == currentGameboard[8] && currentGameboard[8] == USER) ||
                                 (currentGameboard[4] == currentGameboard[6] && currentGameboard[6] == USER) )){
                      return 2;
            }else if( i == 3 && ((currentGameboard[4] == currentGameboard[5] && currentGameboard[5] == USER) ||
                                 (currentGameboard[0] == currentGameboard[6] && currentGameboard[6] == USER) )){
                      return 3;
            }else if( i == 4 && ((currentGameboard[3] == currentGameboard[5] && currentGameboard[5] == USER) ||
                                 (currentGameboard[1] == currentGameboard[7] && currentGameboard[7] == USER) ||
                                 (currentGameboard[0] == currentGameboard[8] && currentGameboard[8] == USER) ||
                                 (currentGameboard[2] == currentGameboard[6] && currentGameboard[6] == USER) )){
                      return 4;
            }else if( i == 5 && ((currentGameboard[3] == currentGameboard[4] && currentGameboard[4] == USER) ||
                                 (currentGameboard[2] == currentGameboard[8] && currentGameboard[8] == USER) )){
                      return 5;
            }else if( i == 6 && ((currentGameboard[7] == currentGameboard[8] && currentGameboard[8] == USER) ||
                                 (currentGameboard[0] == currentGameboard[3] && currentGameboard[3] == USER) ||
                                 (currentGameboard[2] == currentGameboard[4] && currentGameboard[4] == USER) )){
                      return 6;
            }else if( i == 7 && ((currentGameboard[6] == currentGameboard[8] && currentGameboard[8] == USER) ||
                                 (currentGameboard[1] == currentGameboard[4] && currentGameboard[4] == USER) )){
                      return 7;
            }else if( i == 8 && ((currentGameboard[6] == currentGameboard[7] && currentGameboard[7] == USER) ||
                                 (currentGameboard[2] == currentGameboard[5] && currentGameboard[5] == USER) ||
                                 (currentGameboard[0] == currentGameboard[4] && currentGameboard[4] == USER) )){
                      return 8;
            }
        }
    }
    //wenn keines dann random
    while (true){
        int random = rand()%8;
        if (currentGameboard[random]==FREE) return random;
    }
}

/**
 * @brief Gibt den Gewinner zurück wenn einer feststeht.
 *
 * @return int USER,NAO,DRAW oder 0 wenn kein Gewinner feststeht.
 */
int Nicnacnoe::checkWinner(){
    //USER hat gewonnen
    if(     (currentGameboard[0] == currentGameboard[1] && currentGameboard[1] == currentGameboard[2] && currentGameboard[0]==USER) ||
            (currentGameboard[3] == currentGameboard[4] && currentGameboard[4] == currentGameboard[5] && currentGameboard[3]==USER) ||
            (currentGameboard[6] == currentGameboard[7] && currentGameboard[7] == currentGameboard[8] && currentGameboard[6]==USER) ||
            (currentGameboard[0] == currentGameboard[3] && currentGameboard[3] == currentGameboard[6] && currentGameboard[0]==USER) ||
            (currentGameboard[1] == currentGameboard[4] && currentGameboard[4] == currentGameboard[7] && currentGameboard[1]==USER) ||
            (currentGameboard[2] == currentGameboard[5] && currentGameboard[5] == currentGameboard[8] && currentGameboard[2]==USER) ||
            (currentGameboard[0] == currentGameboard[4] && currentGameboard[4] == currentGameboard[8] && currentGameboard[0]==USER) ||
            (currentGameboard[2] == currentGameboard[4] && currentGameboard[4] == currentGameboard[6] && currentGameboard[2]==USER) )
    {
        return USER;
    }
    //NAO hat gewonnen
    else if( (currentGameboard[0] == currentGameboard[1] && currentGameboard[1] == currentGameboard[2] && currentGameboard[0]==NAO) ||
             (currentGameboard[3] == currentGameboard[4] && currentGameboard[4] == currentGameboard[5] && currentGameboard[3]==NAO) ||
             (currentGameboard[6] == currentGameboard[7] && currentGameboard[7] == currentGameboard[8] && currentGameboard[6]==NAO) ||
             (currentGameboard[0] == currentGameboard[3] && currentGameboard[3] == currentGameboard[6] && currentGameboard[0]==NAO) ||
             (currentGameboard[1] == currentGameboard[4] && currentGameboard[4] == currentGameboard[7] && currentGameboard[1]==NAO) ||
             (currentGameboard[2] == currentGameboard[5] && currentGameboard[5] == currentGameboard[8] && currentGameboard[2]==NAO) ||
             (currentGameboard[0] == currentGameboard[4] && currentGameboard[4] == currentGameboard[8] && currentGameboard[0]==NAO) ||
             (currentGameboard[2] == currentGameboard[4] && currentGameboard[4] == currentGameboard[6] && currentGameboard[2]==NAO) )
    {
        return NAO;
    }
    //alle Felder voll
    else if(currentGameboard[0]!=FREE && currentGameboard[1]!=FREE && currentGameboard[2]!=FREE &&
            currentGameboard[3]!=FREE && currentGameboard[4]!=FREE && currentGameboard[5]!=FREE &&
            currentGameboard[6]!=FREE && currentGameboard[7]!=FREE && currentGameboard[8]!=FREE)
    {
        return DRAW;
    }
    else
    {
        return 0;
    }
}

/**
 * @brief Prüft ob das virtuelle Spielfeld leer ist.
 *
 * @return bool TRUE wenn das Spielfeld leer ist.
 */
bool Nicnacnoe::boardEmpty(){
    bool sum = 0;
    for (int i=0 ; i<9 ; i++){
        sum |= currentGameboard[i];
    }
    return !sum;
}

/**
 * @brief Bewegungsablauf von der Init-Position zum Scannen des Spielfeldes.
 */
void Nicnacnoe::motionInitToScan() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(2);
    times.arraySetSize(2);
    keys.arraySetSize(2);

    names.push_back("HeadPitch");
    times[0].arraySetSize(2);
    keys[0].arraySetSize(2);

    times[0][0] = 0.2;
    keys[0][0] = AL::ALValue::array(-0.0195487, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.2, 0));
    times[0][1] = 0.8;
    keys[0][1] = AL::ALValue::array(0.44942, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(2);
    keys[1].arraySetSize(2);

    times[1][0] = 0.2;
    keys[1][0] = AL::ALValue::array(0.012172, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.2, 0));
    times[1][1] = 0.8;
    keys[1][1] = AL::ALValue::array(0.012172, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Bewegungsablauf von der Scann-Position zur Init-Position.
 */
void Nicnacnoe::motionScanToInit() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(2);
    times.arraySetSize(2);
    keys.arraySetSize(2);

    names.push_back("HeadPitch");
    times[0].arraySetSize(2);
    keys[0].arraySetSize(2);

    times[0][0] = 0.2;
    keys[0][0] = AL::ALValue::array(0.449421, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.2, 0));
    times[0][1] = 0.8;
    keys[0][1] = AL::ALValue::array(-0.0195487, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(2);
    keys[1].arraySetSize(2);

    times[1][0] = 0.2;
    keys[1][0] = AL::ALValue::array(0.012172, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.2, 0));
    times[1][1] = 0.8;
    keys[1][1] = AL::ALValue::array(0.012172, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
  * @brief Extrahiert ein Rechteck aus einem Bild.
  *
  * @param pixels Vector der extrahierten Bildpunkte.
  * @param img Array aller Bildpunkte.
  * @param imgWidth Breite des Bildes.
  * @param imgHeight Höhe des Bildes.
  * @param x X-Koordinate der Extrahierung.
  * @param y Y-Koordinate der Extrahierung.
  * @param width Kantenlänge der Extrahierung.
  */
void Nicnacnoe::getPixelRect(std::vector<char> & pixels, const unsigned char* img, int imgWidth, int imgHeight, int x, int y, int width) {

    for (int row = y; row < y + width; row++) {

        for (int col = x; col < x + width; col++) {

            // rauslaufen ... if ((row * width) + col < width * height)
                //pixels->push_back(1);
            int index = (row * imgWidth) + col;
            pixels.push_back(img[index]);
        }
    }
}

/**
 * @brief Ermittelt die Standardabweichung der Pixelwerte innerhalb eines Feldes.
 *
 * @param pixels Vector des Feldes.
 * @return float Wert der ermittelten Standardabweichung.
 */
float Nicnacnoe::getStandardDeviation(std::vector<char> & pixels) {
    float mean = 0;
    for (int i = 0; i < pixels.size(); i++) {
        mean += (int) pixels[i];
    }
    mean /= pixels.size();

    float var = 0;
    for (int i = 0; i < pixels.size(); i++) {
        int pixel = pixels[i];
        var += (pixel - mean) * (pixel - mean);
    }
    var /= pixels.size();

    return sqrt(var);
}

/**
 * @brief Ermittelt den Status eines Feldes.
 *
 * @param img Array aller Bildpunkte.
 * @param width Breite des Bildes.
 * @param height Höhe des Bildes.
 * @param field Nummer des Feldes, welches geprüft werden soll.
 * @return bool true wenn Feld markiert ist.
 */
bool Nicnacnoe::getFieldStatus(const unsigned char* img, int width, int height, int field) {
    std::vector<char> pixels;
    switch (field) {
        case 0:
            getPixelRect(pixels, img, width, height, 248, 104, 45);
            break;
        case 1:
            getPixelRect(pixels, img, width, height, 320, 108, 45);
            break;
        case 2:
            getPixelRect(pixels, img, width, height, 392, 111, 45);
            break;
        case 3:
            getPixelRect(pixels, img, width, height, 243, 172, 45);
            break;
        case 4:
            getPixelRect(pixels, img, width, height, 317, 176, 45);
            break;
        case 5:
            getPixelRect(pixels, img, width, height, 389, 180, 45);
            break;
        case 6:
            getPixelRect(pixels, img, width, height, 236, 248, 45);
            break;
        case 7:
            getPixelRect(pixels, img, width, height, 314, 253, 45);
            break;
        case 8:
            getPixelRect(pixels, img, width, height, 391, 256, 45);
            break;
    }
    return getStandardDeviation(pixels) > 30;
}

/**
 * @brief Debugausgabe des fiktiven Boards.
 */

void Nicnacnoe::printCurrentGameBoard(){
    #ifdef NICNACNOE_IS_REMOTE
    std::cout << "aktuelles Spielfeld" << std::endl;
    std::cout << currentGameboard[0] << " " << currentGameboard[1] << " " << currentGameboard[2] << std::endl;
    std::cout << currentGameboard[3] << " " << currentGameboard[4] << " " << currentGameboard[5] << std::endl;
    std::cout << currentGameboard[6] << " " << currentGameboard[7] << " " << currentGameboard[8] << std::endl;
    #endif
}
