#ifndef NICNACNOE_H
#define NICNACNOE_H

#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <alcommon/almodule.h>
#include <alcommon/alproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alproxies/alrobotpostureproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <alproxies/alvideodeviceproxy.h>
#include <alproxies/alspeechrecognitionproxy.h>
#include <alproxies/alautonomousmovesproxy.h>

using namespace AL;
namespace AL {class ALBroker;}

#define FREE 0
#define NAO 1
#define USER 2
#define DRAW -1

/**
 * @brief Modul, welches das bekannte Spiel TicTacToe implementiert.
 */
class Nicnacnoe  : public AL::ALModule {
private:
    ALMemoryProxy memoryProxy;
    ALTextToSpeechProxy textToSpeechProxy;
    ALVideoDeviceProxy videoDeviceProxy;
    ALSpeechRecognitionProxy speechRecognitionProxy;
    ALAutonomousMovesProxy autonomousMovesProxy;
    boost::shared_ptr<ALProxy> draw;

    std::string robotname; /**< individuell ermittelte Name des Roboter. */
    bool stateWaitToStart; /**< Status zur Unterscheidung ob ein Spiel gestartet wurde. */
    bool forceDoMovement; /**< Status, welcher einen Zug erzwingt auch ohne auf ein Event zu warten. */
    std::string boardCamera; /**< aktuelle Kamera für die Spielfelderkennung */
    int currentGameboard[9]; /**< fiktives Spielfeld, welches den aktuellen Stand wiederspiegelt */

    /**
     * @brief Überschreibt die Init, welche zum Start des naoqi automatisch aufgerufen wird.
     */
    void init();
    /**
     * @brief Wird aufgerufen sobald der Roboter eine Veränderung am Stift feststellt.
     */
    void onGetPen();
    /**
     * @brief Wird aufgerufen, wenn der Tisch sich ändert.
     */
    void onBumperPressed();
    /**
     * @brief Nao kommt an der Reihe und führt seinen Spielzug aus.
     */
    void onGetMovement();
    /**
     * @brief Wird aufgerufen wenn ein Word erkannt wird.
     *
     * @param eventName Name of the event [WordRecognized]
     * @param value Holds the value which word is recognized [index 0] and the confidence of the recognition[index 1]
     * @param subscriberIdentifier The name of the subscriber of the event
     */
    void onWordRecognized(const std::string &eventName, const AL::ALValue &value, const std::string &subscriberIdentifier);
    /**
     * @brief Gibt dem Benutzer eine Hilfe um das leere Spielfeld auszurichten.
     */
    void onCalibrateGameBoard();
    /**
     * @brief Sucht ein neu hinzugekommenes Feld, welches nicht vom nao gekennzeichnet wurde.
     *
     * @return int Die Feldnummer des gefundenen Feldes.
     */
    int findNewField();
    /**
     * @brief Führt einen Spielzug aus.
     */
    void doMovement();
    /**
     * @brief Gibt ein leeres Feld für einen sinnvollen Spielzug zurück.
     *
     * @return int Die Feldnummer, welche gemarkert werden sollte.
     */
    int chooseFreeField();
    /**
     * @brief Gibt den Gewinner zurück wenn einer feststeht.
     *
     * @return int USER,NAO,DRAW oder 0 wenn kein Gewinner feststeht.
     */
    int checkWinner();
    /**
     * @brief Prüft ob das virtuelle Spielfeld leer ist.
     *
     * @return bool TRUE wenn das Spielfeld leer ist.
     */
    bool boardEmpty();
    /**
     * @brief Bewegungsablauf von der Init-Position zum Scannen des Spielfeldes.
     */
    void motionInitToScan();
    /**
     * @brief Bewegungsablauf von der Scann-Position zur Init-Position.
     */
    void motionScanToInit();
    /**
      * @brief Extrahiert ein Rechteck aus einem Bild.
      *
      * @param pixels Vector der extrahierten Bildpunkte.
      * @param img Array aller Bildpunkte.
      * @param imgWidth Breite des Bildes.
      * @param imgHeight Höhe des Bildes.
      * @param x X-Koordinate der Extrahierung.
      * @param y Y-Koordinate der Extrahierung.
      * @param width Kantenlänge der Extrahierung.
      */
    void getPixelRect(std::vector<char> & pixels, const unsigned char* img, int imgWidth, int imgHeight, int x, int y, int width);
    /**
     * @brief Ermittelt die Standardabweichung der Pixelwerte innerhalb eines Feldes.
     *
     * @param pixels Vector des Feldes.
     * @return float Wert der ermittelten Standardabweichung.
     */
    float getStandardDeviation(std::vector<char> & pixels);
    /**
     * @brief Ermittelt den Status eines Feldes.
     *
     * @param img Array aller Bildpunkte.
     * @param width Breite des Bildes.
     * @param height Höhe des Bildes.
     * @param field Nummer des Feldes, welches geprüft werden soll.
     * @return bool true wenn Feld markiert ist.
     */
    bool getFieldStatus(const unsigned char* img, int width, int height, int field);
    /**
     * @brief Debugausgabe des fiktiven Boards.
     */
    void printCurrentGameBoard();
public:
    /**
     * @brief Konstruktor des Moduls.
     *
     * @param broker Der verwendete Broker aus der naoqi.
     * @param name Der Name des Moduls, unter welchem es ansprechbar ist.
     */
    Nicnacnoe(boost::shared_ptr<AL::ALBroker> broker, const std::string &name);
    /**
     * @brief Destruktor des Moduls.
     */
    virtual ~Nicnacnoe();
    /**
     * @brief Startet das Modul und initialisert eine neue Spielrunde.
     */
    void start();
    /**
     * @brief Beendet das laufende Spiel und koppel sich von allen Events ab.
     */
    void stop();
};

#endif // NICNACNOE_H
