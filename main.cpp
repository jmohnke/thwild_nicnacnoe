/*
 * Copyright (c) 2012, 2013 Aldebaran Robotics. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the COPYING file.
 */
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <iostream>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <alcommon/albrokermanager.h>
#include <alproxies/altexttospeechproxy.h>

#include "draw.h"
#include "nicnacnoe.h"

/*
  Um diese Module in locale Module(libthwild_nicnacnoe.so) zu kompilieren:
  1) entfernen Sie den #define NICNACNOE_IS_REMOTE
  2) wechseln Sie in CMakeLists.txt
        - qi_create_bin(thwild_nicnacnoe ${_srcs}) durch
        - qi_create_lib(thwild_nicnacnoe SHARED ${_srcs} SUBFOLDER naoqi)
     aus.
  3) führen Sie erneut "qibuild configure" im Projektordner aus
  4) laden Sie das Projekt mit "qibuild open" erneut
  5) kompilieren Sie das Projekt
  6) kopieren Sie "../build-mytoolchain/sdk/lib/naoqi/libthwild_nicnacnoe.so" mit "scp" auf den Nao nach
     "nao@<NAO IP>:myModules"
  7) Das Modul automatisch beim Bootvorgang des Naos laden über die Datei:
        - naoqi/preferences/autoload.ini
     unter dem Abschnitt [User] die Zeile "/home/nao/myModules/libthwild_nicnacnoe.so" hinzufügen.
*/

#ifdef NICNACNOE_IS_REMOTE
# define ALCALL
#else
# ifdef _WIN32
#  define ALCALL __declspec(dllexport)
# else
#  define ALCALL
# endif
#endif

extern "C"
{
  ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
  {
    // init broker with the main broker instance
    // from the parent executable
    AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(pBroker);
      AL::ALModule::createModule<Draw>( pBroker, "Draw" );
      AL::ALModule::createModule<Nicnacnoe>( pBroker, "Nicnacnoe" );


    return 0;
  }

  ALCALL int _closeModule()
  {
    return 0;
  }
}

#ifdef NICNACNOE_IS_REMOTE
//Remotemodul zum testen
int main(int argc, char *argv[])
{
    int pport = 9559;
    std::string pip = "192.168.0.103";

    // Create your own broker
    boost::shared_ptr<AL::ALBroker> broker = AL::ALBroker::createBroker("nicnacnoebroker", "0.0.0.0", 54000, pip, pport);
    // Deal with ALBrokerManager singleton (add your borker into NAOqi)
    AL::ALBrokerManager::setInstance(broker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(broker);

    // Now it's time to load your module with
    // AL::ALModule::createModule<your_module>(<broker_create>, <your_module>);
    AL::ALModule::createModule <Draw>(broker, "Draw");
    AL::ALModule::createModule <Nicnacnoe>(broker, "Nicnacnoe");

    while (1){

    }
    return 0;
}
#endif //NICNACNOE_IS_REMOTE
