#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <iostream>
#include <alcommon/albroker.h>
#include <qi/os.hpp>
#include "draw.h"
#include "motions.cpp"

/**
 * @brief Konstruktor des Moduls.
 *
 * @param broker Der verwendete Broker aus der naoqi.
 * @param name Der Name des Moduls, unter welchem es ansprechbar ist.
 */
Draw::Draw(boost::shared_ptr<AL::ALBroker> broker, const std::string &name):
    ALModule(broker,name), memoryProxy(getParentBroker()), motionProxy(getParentBroker()), postureProxy(getParentBroker()) {
    functionName("start", getName(), "greife Stift");
        BIND_METHOD(Draw::start);
    functionName("stop", getName(), "gibt Stift frei");
        BIND_METHOD(Draw::stop);
    functionName("drawNicnacnoeField", getName(), "Sestzt ein Zeichen in ein Feld");
        addParam("fieldNumber", "Nummer des Feldes [0..9]");
        BIND_METHOD(Draw::drawNicnacnoeField);
    functionName("toggleHand", getName(), "Öffnet oder schließt die Hand!");
        BIND_METHOD(Draw::toggleHand);

    memoryProxy.declareEvent(std::string("PenInHand"));
}

/**
 * @brief Destruktor des Moduls.
 */
Draw::~Draw() {
    memoryProxy.unsubscribeToEvent("HandRightBackTouched", getName());
}

/**
 * @brief Startes das Modul um eine Position zum Zeichnen einzunehmen und einen Stift in Empfang zu nehmen.
 */
void Draw::start() {
    moveToInit();
    motionInitToGrab();
    //öffne Hand
    memoryProxy.insertData("hand",false);
    motionProxy.openHand("RHand");
    //memoryProxy.raiseEvent("PenInHand", 0);
    //aktiviere Sensor für Nutzereingabe
    memoryProxy.subscribeToEvent("HandRightBackTouched", getName(), "toggleHand");
}

/**
 * @brief Beendet das Modul und koppelt sich von allen Events ab.
 */
void Draw::stop() {
    memoryProxy.insertData("hand",false);
    motionProxy.openHand("RHand");
    memoryProxy.unsubscribeToEvent("HandRightBackTouched", getName());
}

/**
 * @brief Setzt eine Markierung in ein NicNacNoe-Feld.
 *
 * @param fieldNumber Die Feldnummer der Markierung.
 */
void Draw::drawNicnacnoeField(int fieldNumber) {
    switch (fieldNumber) {
        case 0: motionDraw0(); break;
        case 1: motionDraw1(); break;
        case 2: motionDraw2(); break;
        case 3: motionDraw3(); break;
        case 4: motionDraw4(); break;
        case 5: motionDraw5(); break;
        case 6: motionDraw6(); break;
        case 7: motionDraw7(); break;
        case 8: motionDraw8(); break;
    }
}

/**
 * @brief Lässt den Roboter,unabhängig seiner aktuellen Pose, die Position mit dem Namen Init einnehmen.
 */
void Draw::moveToInit(){
    motionProxy.wakeUp(); //stiffnes on
    postureProxy.goToPosture("Stand", 0.5f); // aufstehen
    motionStandToInit();
}

/**
 * @brief Öffnet oder schließt die Hand des Roboters.
 */
void Draw::toggleHand() {
    // smartstiffnes im motionproxy ausschalten um hand nicht wieder zu lockern
    //std::cout << "toogle" << std::endl;
    float touched = memoryProxy.getData("HandRightBackTouched");
    if (touched == 1.0f){
        if (motionProxy.getAngles("RHand",true)[0] < 0.5f) {
            //hand ist geschlossen
            memoryProxy.insertData("hand",false);
            motionProxy.openHand("RHand");
            memoryProxy.raiseEvent("PenInHand", 0);
            motionInitToGrab();
        } else {
            //hand ist offen
            motionProxy.closeHand("RHand");
            memoryProxy.insertData("hand",true);
            memoryProxy.raiseEvent("PenInHand", 1);
            motionGrabToInit();
        }
    }
}
