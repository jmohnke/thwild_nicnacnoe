/**
 * @brief Bewegungsablauf von Stand in die Position mit dem Namen Init.
 */
void Draw::motionStandToInit() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(2);
    keys[0].arraySetSize(2);

    times[0][0] = 0.2;
    keys[0][0] = AL::ALValue::array(-0.145772, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[0][1] = 1.68;
    keys[0][1] = AL::ALValue::array(-0.039926, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(2);
    keys[1].arraySetSize(2);

    times[1][0] = 0.2;
    keys[1][0] = AL::ALValue::array(0.0199001, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[1][1] = 1.68;
    keys[1][1] = AL::ALValue::array(0.0199001, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(2);
    keys[2].arraySetSize(2);

    times[2][0] = 0.2;
    keys[2][0] = AL::ALValue::array(0.0981341, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[2][1] = 1.68;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(2);
    keys[3].arraySetSize(2);

    times[3][0] = 0.2;
    keys[3][0] = AL::ALValue::array(-0.12728, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[3][1] = 1.68;
    keys[3][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(3);
    keys[4].arraySetSize(3);

    times[4][0] = 0.2;
    keys[4][0] = AL::ALValue::array(-0.386526, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[4][1] = 0.96;
    keys[4][1] = AL::ALValue::array(-0.392662, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][2] = 1.68;
    keys[4][2] = AL::ALValue::array(-0.179436, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(3);
    keys[5].arraySetSize(3);

    times[5][0] = 0.2;
    keys[5][0] = AL::ALValue::array(-1.2073, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[5][1] = 0.96;
    keys[5][1] = AL::ALValue::array(-1.20883, AL::ALValue::array(3, -0.253333, 0.00153411), AL::ALValue::array(3, 0.24, -0.00145337));
    times[5][2] = 1.68;
    keys[5][2] = AL::ALValue::array(-1.51717, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(3);
    keys[6].arraySetSize(3);

    times[6][0] = 0.2;
    keys[6][0] = AL::ALValue::array(0.2964, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[6][1] = 0.96;
    keys[6][1] = AL::ALValue::array(0.2944, AL::ALValue::array(3, -0.253333, 0.002), AL::ALValue::array(3, 0.24, -0.00189474));
    times[6][2] = 1.68;
    keys[6][2] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(2);
    keys[7].arraySetSize(2);

    times[7][0] = 0.2;
    keys[7][0] = AL::ALValue::array(0.1335, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[7][1] = 1.68;
    keys[7][1] = AL::ALValue::array(-0.458624, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(2);
    keys[8].arraySetSize(2);

    times[8][0] = 0.2;
    keys[8][0] = AL::ALValue::array(0.0966839, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[8][1] = 1.68;
    keys[8][1] = AL::ALValue::array(0.00617791, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(2);
    keys[9].arraySetSize(2);

    times[9][0] = 0.2;
    keys[9][0] = AL::ALValue::array(-0.162562, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[9][1] = 1.68;
    keys[9][1] = AL::ALValue::array(-0.0152981, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(2);
    keys[10].arraySetSize(2);

    times[10][0] = 0.2;
    keys[10][0] = AL::ALValue::array(-0.0923279, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[10][1] = 1.68;
    keys[10][1] = AL::ALValue::array(0.704064, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(3);
    keys[11].arraySetSize(3);

    times[11][0] = 0.2;
    keys[11][0] = AL::ALValue::array(1.50174, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[11][1] = 0.96;
    keys[11][1] = AL::ALValue::array(1.50021, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][2] = 1.68;
    keys[11][2] = AL::ALValue::array(1.56464, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(3);
    keys[12].arraySetSize(3);

    times[12][0] = 0.2;
    keys[12][0] = AL::ALValue::array(0.168698, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[12][1] = 0.96;
    keys[12][1] = AL::ALValue::array(0.174835, AL::ALValue::array(3, -0.253333, -0.00472625), AL::ALValue::array(3, 0.24, 0.0044775));
    times[12][2] = 1.68;
    keys[12][2] = AL::ALValue::array(0.196309, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(3);
    keys[13].arraySetSize(3);

    times[13][0] = 0.2;
    keys[13][0] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[13][1] = 0.96;
    keys[13][1] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][2] = 1.68;
    keys[13][2] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(2);
    keys[14].arraySetSize(2);

    times[14][0] = 0.2;
    keys[14][0] = AL::ALValue::array(0.0966839, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[14][1] = 1.68;
    keys[14][1] = AL::ALValue::array(-0.360449, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(2);
    keys[15].arraySetSize(2);

    times[15][0] = 0.2;
    keys[15][0] = AL::ALValue::array(0.122762, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[15][1] = 1.68;
    keys[15][1] = AL::ALValue::array(0.00464392, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(3);
    keys[16].arraySetSize(3);

    times[16][0] = 0.2;
    keys[16][0] = AL::ALValue::array(0.428028, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[16][1] = 0.96;
    keys[16][1] = AL::ALValue::array(1.52177, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][2] = 1.68;
    keys[16][2] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(3);
    keys[17].arraySetSize(3);

    times[17][0] = 0.2;
    keys[17][0] = AL::ALValue::array(1.21028, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[17][1] = 0.96;
    keys[17][1] = AL::ALValue::array(1.53549, AL::ALValue::array(3, -0.253333, -0.0950527), AL::ALValue::array(3, 0.24, 0.0900499));
    times[17][2] = 1.68;
    keys[17][2] = AL::ALValue::array(1.76559, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(3);
    keys[18].arraySetSize(3);

    times[18][0] = 0.2;
    keys[18][0] = AL::ALValue::array(0.296, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[18][1] = 0.96;
    keys[18][1] = AL::ALValue::array(0.2932, AL::ALValue::array(3, -0.253333, 0.0023964), AL::ALValue::array(3, 0.24, -0.00227027));
    times[18][2] = 1.68;
    keys[18][2] = AL::ALValue::array(0.282, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(2);
    keys[19].arraySetSize(2);

    times[19][0] = 0.2;
    keys[19][0] = AL::ALValue::array(0.133416, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[19][1] = 1.68;
    keys[19][1] = AL::ALValue::array(-0.458707, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(2);
    keys[20].arraySetSize(2);

    times[20][0] = 0.2;
    keys[20][0] = AL::ALValue::array(-0.095066, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[20][1] = 1.68;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(2);
    keys[21].arraySetSize(2);

    times[21][0] = 0.2;
    keys[21][0] = AL::ALValue::array(-0.162562, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[21][1] = 1.68;
    keys[21][1] = AL::ALValue::array(-0.0152981, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(2);
    keys[22].arraySetSize(2);

    times[22][0] = 0.2;
    keys[22][0] = AL::ALValue::array(-0.091998, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.493333, 0));
    times[22][1] = 1.68;
    keys[22][1] = AL::ALValue::array(0.707216, AL::ALValue::array(3, -0.493333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(3);
    keys[23].arraySetSize(3);

    times[23][0] = 0.2;
    keys[23][0] = AL::ALValue::array(1.46348, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[23][1] = 0.96;
    keys[23][1] = AL::ALValue::array(1.96816, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][2] = 1.68;
    keys[23][2] = AL::ALValue::array(1.03242, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(3);
    keys[24].arraySetSize(3);

    times[24][0] = 0.2;
    keys[24][0] = AL::ALValue::array(-0.153442, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[24][1] = 0.96;
    keys[24][1] = AL::ALValue::array(-0.283832, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][2] = 1.68;
    keys[24][2] = AL::ALValue::array(-0.108956, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(3);
    keys[25].arraySetSize(3);

    times[25][0] = 0.2;
    keys[25][0] = AL::ALValue::array(0.076658, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.253333, 0));
    times[25][1] = 0.96;
    keys[25][1] = AL::ALValue::array(0.093532, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][2] = 1.68;
    keys[25][2] = AL::ALValue::array(0.061318, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Bewegungsablauf von der Position Init zur Position um einen Stift in Empfang zu nehmen.
 */
void Draw::motionInitToGrab() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(2);
    keys[0].arraySetSize(2);

    times[0][0] = 0.2;
    keys[0][0] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[0][1] = 1.32;
    keys[0][1] = AL::ALValue::array(-0.329852, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(2);
    keys[1].arraySetSize(2);

    times[1][0] = 0.2;
    keys[1][0] = AL::ALValue::array(0.00916195, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[1][1] = 1.32;
    keys[1][1] = AL::ALValue::array(-0.866751, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(2);
    keys[2].arraySetSize(2);

    times[2][0] = 0.2;
    keys[2][0] = AL::ALValue::array(-0.339056, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[2][1] = 1.32;
    keys[2][1] = AL::ALValue::array(-0.339056, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(2);
    keys[3].arraySetSize(2);

    times[3][0] = 0.2;
    keys[3][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[3][1] = 1.32;
    keys[3][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(2);
    keys[4].arraySetSize(2);

    times[4][0] = 0.2;
    keys[4][0] = AL::ALValue::array(-0.176367, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[4][1] = 1.32;
    keys[4][1] = AL::ALValue::array(-0.176367, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(2);
    keys[5].arraySetSize(2);

    times[5][0] = 0.2;
    keys[5][0] = AL::ALValue::array(-1.52484, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[5][1] = 1.32;
    keys[5][1] = AL::ALValue::array(-1.52484, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(2);
    keys[6].arraySetSize(2);

    times[6][0] = 0.2;
    keys[6][0] = AL::ALValue::array(0.2616, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[6][1] = 1.32;
    keys[6][1] = AL::ALValue::array(0.2616, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(2);
    keys[7].arraySetSize(2);

    times[7][0] = 0.2;
    keys[7][0] = AL::ALValue::array(-0.461692, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[7][1] = 1.32;
    keys[7][1] = AL::ALValue::array(-0.461692, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(2);
    keys[8].arraySetSize(2);

    times[8][0] = 0.2;
    keys[8][0] = AL::ALValue::array(0.00310993, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[8][1] = 1.32;
    keys[8][1] = AL::ALValue::array(0.00310993, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(2);
    keys[9].arraySetSize(2);

    times[9][0] = 0.2;
    keys[9][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[9][1] = 1.32;
    keys[9][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(2);
    keys[10].arraySetSize(2);

    times[10][0] = 0.2;
    keys[10][0] = AL::ALValue::array(0.708667, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[10][1] = 1.32;
    keys[10][1] = AL::ALValue::array(0.708667, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(2);
    keys[11].arraySetSize(2);

    times[11][0] = 0.2;
    keys[11][0] = AL::ALValue::array(1.5631, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[11][1] = 1.32;
    keys[11][1] = AL::ALValue::array(1.5631, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(2);
    keys[12].arraySetSize(2);

    times[12][0] = 0.2;
    keys[12][0] = AL::ALValue::array(0.222388, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[12][1] = 1.32;
    keys[12][1] = AL::ALValue::array(0.222388, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(2);
    keys[13].arraySetSize(2);

    times[13][0] = 0.2;
    keys[13][0] = AL::ALValue::array(0.10427, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[13][1] = 1.32;
    keys[13][1] = AL::ALValue::array(0.10427, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(2);
    keys[14].arraySetSize(2);

    times[14][0] = 0.2;
    keys[14][0] = AL::ALValue::array(-0.348176, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[14][1] = 1.32;
    keys[14][1] = AL::ALValue::array(-0.348176, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(2);
    keys[15].arraySetSize(2);

    times[15][0] = 0.2;
    keys[15][0] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[15][1] = 1.32;
    keys[15][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(2);
    keys[16].arraySetSize(2);

    times[16][0] = 0.2;
    keys[16][0] = AL::ALValue::array(1.31775, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[16][1] = 1.32;
    keys[16][1] = AL::ALValue::array(0.836072, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(2);
    keys[17].arraySetSize(2);

    times[17][0] = 0.2;
    keys[17][0] = AL::ALValue::array(1.76559, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[17][1] = 1.32;
    keys[17][1] = AL::ALValue::array(2.0816, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(2);
    keys[18].arraySetSize(2);

    times[18][0] = 0.2;
    keys[18][0] = AL::ALValue::array(0.2708, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[18][1] = 1.32;
    keys[18][1] = AL::ALValue::array(0.2708, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(2);
    keys[19].arraySetSize(2);

    times[19][0] = 0.2;
    keys[19][0] = AL::ALValue::array(-0.458707, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[19][1] = 1.32;
    keys[19][1] = AL::ALValue::array(-0.458707, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(2);
    keys[20].arraySetSize(2);

    times[20][0] = 0.2;
    keys[20][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[20][1] = 1.32;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(2);
    keys[21].arraySetSize(2);

    times[21][0] = 0.2;
    keys[21][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[21][1] = 1.32;
    keys[21][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(2);
    keys[22].arraySetSize(2);

    times[22][0] = 0.2;
    keys[22][0] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[22][1] = 1.32;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(2);
    keys[23].arraySetSize(2);

    times[23][0] = 0.2;
    keys[23][0] = AL::ALValue::array(1.03703, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[23][1] = 1.32;
    keys[23][1] = AL::ALValue::array(0.757838, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(2);
    keys[24].arraySetSize(2);

    times[24][0] = 0.2;
    keys[24][0] = AL::ALValue::array(-0.099752, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[24][1] = 1.32;
    keys[24][1] = AL::ALValue::array(-0.699545, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(2);
    keys[25].arraySetSize(2);

    times[25][0] = 0.2;
    keys[25][0] = AL::ALValue::array(0.061318, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.373333, 0));
    times[25][1] = 1.32;
    keys[25][1] = AL::ALValue::array(0.061318, AL::ALValue::array(3, -0.373333, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Bewegungsablauf von dem Empfang eines Stiftes zurück zur Position mit dem Namen Init.
 */
void Draw::motionGrabToInit() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(2);
    keys[0].arraySetSize(2);

    times[0][0] = 0.2;
    keys[0][0] = AL::ALValue::array(-0.329852, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[0][1] = 1.28;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(2);
    keys[1].arraySetSize(2);

    times[1][0] = 0.2;
    keys[1][0] = AL::ALValue::array(-0.866751, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[1][1] = 1.28;
    keys[1][1] = AL::ALValue::array(0.00916195, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(2);
    keys[2].arraySetSize(2);

    times[2][0] = 0.2;
    keys[2][0] = AL::ALValue::array(-0.339056, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[2][1] = 1.28;
    keys[2][1] = AL::ALValue::array(-0.339056, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(2);
    keys[3].arraySetSize(2);

    times[3][0] = 0.2;
    keys[3][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[3][1] = 1.28;
    keys[3][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(2);
    keys[4].arraySetSize(2);

    times[4][0] = 0.2;
    keys[4][0] = AL::ALValue::array(-0.176367, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[4][1] = 1.28;
    keys[4][1] = AL::ALValue::array(-0.176367, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(2);
    keys[5].arraySetSize(2);

    times[5][0] = 0.2;
    keys[5][0] = AL::ALValue::array(-1.52484, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[5][1] = 1.28;
    keys[5][1] = AL::ALValue::array(-1.52484, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(2);
    keys[6].arraySetSize(2);

    times[6][0] = 0.2;
    keys[6][0] = AL::ALValue::array(0.2616, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[6][1] = 1.28;
    keys[6][1] = AL::ALValue::array(0.2616, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(2);
    keys[7].arraySetSize(2);

    times[7][0] = 0.2;
    keys[7][0] = AL::ALValue::array(-0.461692, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[7][1] = 1.28;
    keys[7][1] = AL::ALValue::array(-0.461692, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(2);
    keys[8].arraySetSize(2);

    times[8][0] = 0.2;
    keys[8][0] = AL::ALValue::array(0.00310993, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[8][1] = 1.28;
    keys[8][1] = AL::ALValue::array(0.00310993, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(2);
    keys[9].arraySetSize(2);

    times[9][0] = 0.2;
    keys[9][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[9][1] = 1.28;
    keys[9][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(2);
    keys[10].arraySetSize(2);

    times[10][0] = 0.2;
    keys[10][0] = AL::ALValue::array(0.708667, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[10][1] = 1.28;
    keys[10][1] = AL::ALValue::array(0.708667, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(2);
    keys[11].arraySetSize(2);

    times[11][0] = 0.2;
    keys[11][0] = AL::ALValue::array(1.5631, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[11][1] = 1.28;
    keys[11][1] = AL::ALValue::array(1.5631, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(2);
    keys[12].arraySetSize(2);

    times[12][0] = 0.2;
    keys[12][0] = AL::ALValue::array(0.222388, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[12][1] = 1.28;
    keys[12][1] = AL::ALValue::array(0.222388, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(2);
    keys[13].arraySetSize(2);

    times[13][0] = 0.2;
    keys[13][0] = AL::ALValue::array(0.10427, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[13][1] = 1.28;
    keys[13][1] = AL::ALValue::array(0.10427, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(2);
    keys[14].arraySetSize(2);

    times[14][0] = 0.2;
    keys[14][0] = AL::ALValue::array(-0.348176, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[14][1] = 1.28;
    keys[14][1] = AL::ALValue::array(-0.348176, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(2);
    keys[15].arraySetSize(2);

    times[15][0] = 0.2;
    keys[15][0] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[15][1] = 1.28;
    keys[15][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(2);
    keys[16].arraySetSize(2);

    times[16][0] = 0.2;
    keys[16][0] = AL::ALValue::array(0.836072, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[16][1] = 1.28;
    keys[16][1] = AL::ALValue::array(1.31775, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(2);
    keys[17].arraySetSize(2);

    times[17][0] = 0.2;
    keys[17][0] = AL::ALValue::array(2.0816, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[17][1] = 1.28;
    keys[17][1] = AL::ALValue::array(1.76559, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(2);
    keys[18].arraySetSize(2);

    times[18][0] = 0.2;
    keys[18][0] = AL::ALValue::array(0.2708, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[18][1] = 1.28;
    keys[18][1] = AL::ALValue::array(0.2708, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(2);
    keys[19].arraySetSize(2);

    times[19][0] = 0.2;
    keys[19][0] = AL::ALValue::array(-0.458707, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[19][1] = 1.28;
    keys[19][1] = AL::ALValue::array(-0.458707, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(2);
    keys[20].arraySetSize(2);

    times[20][0] = 0.2;
    keys[20][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[20][1] = 1.28;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(2);
    keys[21].arraySetSize(2);

    times[21][0] = 0.2;
    keys[21][0] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[21][1] = 1.28;
    keys[21][1] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(2);
    keys[22].arraySetSize(2);

    times[22][0] = 0.2;
    keys[22][0] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[22][1] = 1.28;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(2);
    keys[23].arraySetSize(2);

    times[23][0] = 0.2;
    keys[23][0] = AL::ALValue::array(0.757838, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[23][1] = 1.28;
    keys[23][1] = AL::ALValue::array(1.03703, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(2);
    keys[24].arraySetSize(2);

    times[24][0] = 0.2;
    keys[24][0] = AL::ALValue::array(-0.699545, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[24][1] = 1.28;
    keys[24][1] = AL::ALValue::array(-0.099752, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(2);
    keys[25].arraySetSize(2);

    times[25][0] = 0.2;
    keys[25][0] = AL::ALValue::array(0.061318, AL::ALValue::array(3, -0.0666667, 0), AL::ALValue::array(3, 0.36, 0));
    times[25][1] = 1.28;
    keys[25][1] = AL::ALValue::array(0.061318, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 0
 */
void Draw::motionDraw0() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(4);
    keys[0].arraySetSize(4);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][1] = 0.88;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[0][2] = 2.48;
    keys[0][2] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][3] = 3.2;
    keys[0][3] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(4);
    keys[1].arraySetSize(4);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][1] = 0.88;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[1][2] = 2.48;
    keys[1][2] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][3] = 3.2;
    keys[1][3] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(4);
    keys[2].arraySetSize(4);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.34059, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[2][2] = 2.48;
    keys[2][2] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][3] = 3.2;
    keys[2][3] = AL::ALValue::array(-0.34059, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(4);
    keys[3].arraySetSize(4);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[3][2] = 2.48;
    keys[3][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][3] = 3.2;
    keys[3][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(6);
    keys[4].arraySetSize(6);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174834, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.24, 1.49803e-07), AL::ALValue::array(3, 0.133333, -8.32238e-08));
    times[4][2] = 1.28;
    keys[4][2] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[4][3] = 1.64;
    keys[4][3] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.28, 0));
    times[4][4] = 2.48;
    keys[4][4] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.28, -8.32238e-08), AL::ALValue::array(3, 0.24, 7.13347e-08));
    times[4][5] = 3.2;
    keys[4][5] = AL::ALValue::array(-0.174834, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(6);
    keys[5].arraySetSize(6);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, -2.39685e-07), AL::ALValue::array(3, 0.133333, 1.33158e-07));
    times[5][2] = 1.28;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[5][3] = 1.64;
    keys[5][3] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.28, 0));
    times[5][4] = 2.48;
    keys[5][4] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.28, 1.33158e-07), AL::ALValue::array(3, 0.24, -1.14135e-07));
    times[5][5] = 3.2;
    keys[5][5] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(6);
    keys[6].arraySetSize(6);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[6][2] = 1.28;
    keys[6][2] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[6][3] = 1.64;
    keys[6][3] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.28, 0));
    times[6][4] = 2.48;
    keys[6][4] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][5] = 3.2;
    keys[6][5] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(4);
    keys[7].arraySetSize(4);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[7][2] = 2.48;
    keys[7][2] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][3] = 3.2;
    keys[7][3] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(4);
    keys[8].arraySetSize(4);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[8][2] = 2.48;
    keys[8][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][3] = 3.2;
    keys[8][3] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(4);
    keys[9].arraySetSize(4);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[9][2] = 2.48;
    keys[9][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][3] = 3.2;
    keys[9][3] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(4);
    keys[10].arraySetSize(4);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.7102, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[10][2] = 2.48;
    keys[10][2] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][3] = 3.2;
    keys[10][3] = AL::ALValue::array(0.7102, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(6);
    keys[11].arraySetSize(6);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.24, 0.0131486), AL::ALValue::array(3, 0.133333, -0.00730477));
    times[11][2] = 1.28;
    keys[11][2] = AL::ALValue::array(1.51402, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[11][3] = 1.64;
    keys[11][3] = AL::ALValue::array(1.52322, AL::ALValue::array(3, -0.12, -0.00429521), AL::ALValue::array(3, 0.28, 0.0100222));
    times[11][4] = 2.48;
    keys[11][4] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.28, -0.00936134), AL::ALValue::array(3, 0.24, 0.008024));
    times[11][5] = 3.2;
    keys[11][5] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(6);
    keys[12].arraySetSize(6);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214718, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.24, -0.00723169), AL::ALValue::array(3, 0.133333, 0.00401761));
    times[12][2] = 1.28;
    keys[12][2] = AL::ALValue::array(0.248466, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[12][3] = 1.64;
    keys[12][3] = AL::ALValue::array(0.240796, AL::ALValue::array(3, -0.12, 0.00122724), AL::ALValue::array(3, 0.28, -0.00286355));
    times[12][4] = 2.48;
    keys[12][4] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.28, 0.00460242), AL::ALValue::array(3, 0.24, -0.00394494));
    times[12][5] = 3.2;
    keys[12][5] = AL::ALValue::array(0.214718, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(6);
    keys[13].arraySetSize(6);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, -1.12352e-07), AL::ALValue::array(3, 0.133333, 6.24178e-08));
    times[13][2] = 1.28;
    keys[13][2] = AL::ALValue::array(0.053648, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[13][3] = 1.64;
    keys[13][3] = AL::ALValue::array(0.053648, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.28, 0));
    times[13][4] = 2.48;
    keys[13][4] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.28, 6.24178e-08), AL::ALValue::array(3, 0.24, -5.3501e-08));
    times[13][5] = 3.2;
    keys[13][5] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(4);
    keys[14].arraySetSize(4);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[14][2] = 2.48;
    keys[14][2] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][3] = 3.2;
    keys[14][3] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(4);
    keys[15].arraySetSize(4);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[15][2] = 2.48;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][3] = 3.2;
    keys[15][3] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(6);
    keys[16].arraySetSize(6);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[16][2] = 1.28;
    keys[16][2] = AL::ALValue::array(1.11526, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[16][3] = 1.64;
    keys[16][3] = AL::ALValue::array(1.12293, AL::ALValue::array(3, -0.12, -0.00766997), AL::ALValue::array(3, 0.28, 0.0178966));
    times[16][4] = 2.48;
    keys[16][4] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][5] = 3.2;
    keys[16][5] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(6);
    keys[17].arraySetSize(6);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[17][2] = 1.28;
    keys[17][2] = AL::ALValue::array(1.12745, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[17][3] = 1.64;
    keys[17][3] = AL::ALValue::array(1.12591, AL::ALValue::array(3, -0.12, 0.00153398), AL::ALValue::array(3, 0.28, -0.00357929));
    times[17][4] = 2.48;
    keys[17][4] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][5] = 3.2;
    keys[17][5] = AL::ALValue::array(1.77019, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(6);
    keys[18].arraySetSize(6);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[18][2] = 1.28;
    keys[18][2] = AL::ALValue::array(0.19, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[18][3] = 1.64;
    keys[18][3] = AL::ALValue::array(0.1896, AL::ALValue::array(3, -0.12, 0.000400007), AL::ALValue::array(3, 0.28, -0.000933349));
    times[18][4] = 2.48;
    keys[18][4] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][5] = 3.2;
    keys[18][5] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(4);
    keys[19].arraySetSize(4);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.45564, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[19][2] = 2.48;
    keys[19][2] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][3] = 3.2;
    keys[19][3] = AL::ALValue::array(-0.45564, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(4);
    keys[20].arraySetSize(4);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[20][2] = 2.48;
    keys[20][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][3] = 3.2;
    keys[20][3] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(4);
    keys[21].arraySetSize(4);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[21][2] = 2.48;
    keys[21][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][3] = 3.2;
    keys[21][3] = AL::ALValue::array(-0.00302601, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(4);
    keys[22].arraySetSize(4);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[22][2] = 2.48;
    keys[22][2] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][3] = 3.2;
    keys[22][3] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(6);
    keys[23].arraySetSize(6);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[23][2] = 1.28;
    keys[23][2] = AL::ALValue::array(0.713352, AL::ALValue::array(3, -0.133333, -0.0487112), AL::ALValue::array(3, 0.12, 0.0438401));
    times[23][3] = 1.64;
    keys[23][3] = AL::ALValue::array(0.857548, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.28, 0));
    times[23][4] = 2.48;
    keys[23][4] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][5] = 3.2;
    keys[23][5] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(6);
    keys[24].arraySetSize(6);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.133333, 0));
    times[24][2] = 1.28;
    keys[24][2] = AL::ALValue::array(0.193242, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[24][3] = 1.64;
    keys[24][3] = AL::ALValue::array(0.205514, AL::ALValue::array(3, -0.12, -0.00306802), AL::ALValue::array(3, 0.28, 0.00715871));
    times[24][4] = 2.48;
    keys[24][4] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.28, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][5] = 3.2;
    keys[24][5] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(6);
    keys[25].arraySetSize(6);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(0.247837, AL::ALValue::array(3, -0.24, -0.13806), AL::ALValue::array(3, 0.133333, 0.0767));
    times[25][2] = 1.28;
    keys[25][2] = AL::ALValue::array(0.708604, AL::ALValue::array(3, -0.133333, 0), AL::ALValue::array(3, 0.12, 0));
    times[25][3] = 1.64;
    keys[25][3] = AL::ALValue::array(0.608956, AL::ALValue::array(3, -0.12, 0.0497015), AL::ALValue::array(3, 0.28, -0.11597));
    times[25][4] = 2.48;
    keys[25][4] = AL::ALValue::array(0.211651, AL::ALValue::array(3, -0.28, 0.0988447), AL::ALValue::array(3, 0.24, -0.084724));
    times[25][5] = 3.2;
    keys[25][5] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 1
 */
void Draw::motionDraw1() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(5);
    keys[0].arraySetSize(5);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.12, 0));
    times[0][1] = 0.52;
    keys[0][1] = AL::ALValue::array(0, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[0][2] = 0.88;
    keys[0][2] = AL::ALValue::array(0, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.533333, 0));
    times[0][3] = 2.48;
    keys[0][3] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][4] = 3.2;
    keys[0][4] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(5);
    keys[1].arraySetSize(5);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.12, 0));
    times[1][1] = 0.52;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[1][2] = 0.88;
    keys[1][2] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.533333, 0));
    times[1][3] = 2.48;
    keys[1][3] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][4] = 3.2;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(4);
    keys[2].arraySetSize(4);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 2.99606e-08), AL::ALValue::array(3, 0.533333, -6.6579e-08));
    times[2][2] = 2.48;
    keys[2][2] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][3] = 3.2;
    keys[2][3] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(4);
    keys[3].arraySetSize(4);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[3][2] = 2.48;
    keys[3][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][3] = 3.2;
    keys[3][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(5);
    keys[4].arraySetSize(5);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.191708, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][2] = 1.6;
    keys[4][2] = AL::ALValue::array(-0.18097, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[4][3] = 2.48;
    keys[4][3] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][4] = 3.2;
    keys[4][4] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(5);
    keys[5].arraySetSize(5);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.51257, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][2] = 1.6;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[5][3] = 2.48;
    keys[5][3] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][4] = 3.2;
    keys[5][4] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(5);
    keys[6].arraySetSize(5);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.24, -0.00119999), AL::ALValue::array(3, 0.24, 0.00119999));
    times[6][2] = 1.6;
    keys[6][2] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[6][3] = 2.48;
    keys[6][3] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][4] = 3.2;
    keys[6][4] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(4);
    keys[7].arraySetSize(4);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.704064, AL::ALValue::array(3, -0.24, 0.000690321), AL::ALValue::array(3, 0.533333, -0.00153405));
    times[7][2] = 2.48;
    keys[7][2] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][3] = 3.2;
    keys[7][3] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(4);
    keys[8].arraySetSize(4);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(0, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[8][2] = 2.48;
    keys[8][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][3] = 3.2;
    keys[8][3] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(4);
    keys[9].arraySetSize(4);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00609398, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[9][2] = 2.48;
    keys[9][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][3] = 3.2;
    keys[9][3] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(4);
    keys[10].arraySetSize(4);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696394, AL::ALValue::array(3, -0.24, 2.99606e-07), AL::ALValue::array(3, 0.533333, -6.6579e-07));
    times[10][2] = 2.48;
    keys[10][2] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][3] = 3.2;
    keys[10][3] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(5);
    keys[11].arraySetSize(5);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.54009, AL::ALValue::array(3, -0.24, 0.00894829), AL::ALValue::array(3, 0.24, -0.00894829));
    times[11][2] = 1.6;
    keys[11][2] = AL::ALValue::array(1.52169, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[11][3] = 2.48;
    keys[11][3] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.293333, -0.00984312), AL::ALValue::array(3, 0.24, 0.00805346));
    times[11][4] = 3.2;
    keys[11][4] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(5);
    keys[12].arraySetSize(5);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.253068, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][2] = 1.6;
    keys[12][2] = AL::ALValue::array(0.248467, AL::ALValue::array(3, -0.24, 0.00253115), AL::ALValue::array(3, 0.293333, -0.00309363));
    times[12][3] = 2.48;
    keys[12][3] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.293333, 0.0061874), AL::ALValue::array(3, 0.24, -0.00506242));
    times[12][4] = 3.2;
    keys[12][4] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(5);
    keys[13].arraySetSize(5);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.0889301, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][2] = 1.6;
    keys[13][2] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[13][3] = 2.48;
    keys[13][3] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][4] = 3.2;
    keys[13][4] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(4);
    keys[14].arraySetSize(4);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 1.49803e-08), AL::ALValue::array(3, 0.533333, -3.32895e-08));
    times[14][2] = 2.48;
    keys[14][2] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][3] = 3.2;
    keys[14][3] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(4);
    keys[15].arraySetSize(4);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00924587, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[15][2] = 2.48;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.533333, 0.00153398), AL::ALValue::array(3, 0.24, -0.00069029));
    times[15][3] = 3.2;
    keys[15][3] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(5);
    keys[16].arraySetSize(5);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.46808, AL::ALValue::array(3, -0.24, -0.0327255), AL::ALValue::array(3, 0.24, 0.0327255));
    times[16][2] = 1.6;
    keys[16][2] = AL::ALValue::array(1.51257, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[16][3] = 2.48;
    keys[16][3] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.293333, 0.0362793), AL::ALValue::array(3, 0.24, -0.0296831));
    times[16][4] = 3.2;
    keys[16][4] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(5);
    keys[17].arraySetSize(5);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.20722, AL::ALValue::array(3, -0.24, 0.0920396), AL::ALValue::array(3, 0.24, -0.0920396));
    times[17][2] = 1.6;
    keys[17][2] = AL::ALValue::array(1.11518, AL::ALValue::array(3, -0.24, 0.00251042), AL::ALValue::array(3, 0.293333, -0.00306829));
    times[17][3] = 2.48;
    keys[17][3] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][4] = 3.2;
    keys[17][4] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(5);
    keys[18].arraySetSize(5);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.212, AL::ALValue::array(3, -0.24, 0.0100667), AL::ALValue::array(3, 0.24, -0.0100667));
    times[18][2] = 1.6;
    keys[18][2] = AL::ALValue::array(0.1936, AL::ALValue::array(3, -0.24, 0.00522), AL::ALValue::array(3, 0.293333, -0.00638));
    times[18][3] = 2.48;
    keys[18][3] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][4] = 3.2;
    keys[18][4] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(4);
    keys[19].arraySetSize(4);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 2.09724e-07), AL::ALValue::array(3, 0.533333, -4.66053e-07));
    times[19][2] = 2.48;
    keys[19][2] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][3] = 3.2;
    keys[19][3] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(4);
    keys[20].arraySetSize(4);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(0, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[20][2] = 2.48;
    keys[20][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][3] = 3.2;
    keys[20][3] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(4);
    keys[21].arraySetSize(4);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00609398, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[21][2] = 2.48;
    keys[21][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][3] = 3.2;
    keys[21][3] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(4);
    keys[22].arraySetSize(4);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.704148, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.533333, 0));
    times[22][2] = 2.48;
    keys[22][2] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][3] = 3.2;
    keys[22][3] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(5);
    keys[23].arraySetSize(5);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(0.957258, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][2] = 1.6;
    keys[23][2] = AL::ALValue::array(1.02322, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.293333, 0));
    times[23][3] = 2.48;
    keys[23][3] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][4] = 3.2;
    keys[23][4] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(5);
    keys[24].arraySetSize(5);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.105804, AL::ALValue::array(3, -0.24, -0.039117), AL::ALValue::array(3, 0.24, 0.039117));
    times[24][2] = 1.6;
    keys[24][2] = AL::ALValue::array(0.153358, AL::ALValue::array(3, -0.24, -0.0177177), AL::ALValue::array(3, 0.293333, 0.021655));
    times[24][3] = 2.48;
    keys[24][3] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.293333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][4] = 3.2;
    keys[24][4] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(5);
    keys[25].arraySetSize(5);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(1.01086, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][2] = 1.6;
    keys[25][2] = AL::ALValue::array(0.286234, AL::ALValue::array(3, -0.24, 0.0610226), AL::ALValue::array(3, 0.293333, -0.0745831));
    times[25][3] = 2.48;
    keys[25][3] = AL::ALValue::array(0.211651, AL::ALValue::array(3, -0.293333, 0.0417971), AL::ALValue::array(3, 0.24, -0.0341976));
    times[25][4] = 3.2;
    keys[25][4] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 2
 */
void Draw::motionDraw2() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(5);
    keys[0].arraySetSize(5);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][1] = 0.88;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.24, 0.00212426), AL::ALValue::array(3, 0.173333, -0.00153419));
    times[0][2] = 1.4;
    keys[0][2] = AL::ALValue::array(-0.0368581, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[0][3] = 2.48;
    keys[0][3] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.36, -0.00153419), AL::ALValue::array(3, 0.24, 0.00102279));
    times[0][4] = 3.2;
    keys[0][4] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(5);
    keys[1].arraySetSize(5);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][1] = 0.88;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[1][2] = 1.4;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[1][3] = 2.48;
    keys[1][3] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][4] = 3.2;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(5);
    keys[2].arraySetSize(5);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 0.00326585), AL::ALValue::array(3, 0.173333, -0.00235867));
    times[2][2] = 1.4;
    keys[2][2] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[2][3] = 2.48;
    keys[2][3] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.36, -0.00337471), AL::ALValue::array(3, 0.24, 0.00224981));
    times[2][4] = 3.2;
    keys[2][4] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(5);
    keys[3].arraySetSize(5);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[3][2] = 1.4;
    keys[3][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[3][3] = 2.48;
    keys[3][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][4] = 3.2;
    keys[3][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(6);
    keys[4].arraySetSize(6);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.24, 0.00267199), AL::ALValue::array(3, 0.173333, -0.00192977));
    times[4][2] = 1.4;
    keys[4][2] = AL::ALValue::array(-0.18864, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[4][3] = 1.88;
    keys[4][3] = AL::ALValue::array(-0.18864, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[4][4] = 2.48;
    keys[4][4] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.2, -0.00209171), AL::ALValue::array(3, 0.24, 0.00251005));
    times[4][5] = 3.2;
    keys[4][5] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(6);
    keys[5].arraySetSize(6);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[5][2] = 1.4;
    keys[5][2] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[5][3] = 1.88;
    keys[5][3] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[5][4] = 2.48;
    keys[5][4] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][5] = 3.2;
    keys[5][5] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(6);
    keys[6].arraySetSize(6);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[6][2] = 1.4;
    keys[6][2] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[6][3] = 1.88;
    keys[6][3] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[6][4] = 2.48;
    keys[6][4] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][5] = 3.2;
    keys[6][5] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(5);
    keys[7].arraySetSize(5);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.24, 0.0084958), AL::ALValue::array(3, 0.173333, -0.00613586));
    times[7][2] = 1.4;
    keys[7][2] = AL::ALValue::array(-0.711734, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[7][3] = 2.48;
    keys[7][3] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.36, -0.00613586), AL::ALValue::array(3, 0.24, 0.00409057));
    times[7][4] = 3.2;
    keys[7][4] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(5);
    keys[8].arraySetSize(5);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 6.48185e-09), AL::ALValue::array(3, 0.173333, -4.68134e-09));
    times[8][2] = 1.4;
    keys[8][2] = AL::ALValue::array(-0.00609398, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[8][3] = 2.48;
    keys[8][3] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.36, -4.68134e-09), AL::ALValue::array(3, 0.24, 3.12089e-09));
    times[8][4] = 3.2;
    keys[8][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(5);
    keys[9].arraySetSize(5);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[9][2] = 1.4;
    keys[9][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[9][3] = 2.48;
    keys[9][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][4] = 3.2;
    keys[9][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(5);
    keys[10].arraySetSize(5);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.24, 0.00356296), AL::ALValue::array(3, 0.173333, -0.00257325));
    times[10][2] = 1.4;
    keys[10][2] = AL::ALValue::array(0.691792, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[10][3] = 2.48;
    keys[10][3] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.36, -0.00368173), AL::ALValue::array(3, 0.24, 0.00245448));
    times[10][4] = 3.2;
    keys[10][4] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(6);
    keys[11].arraySetSize(6);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.24, 0.0145483), AL::ALValue::array(3, 0.173333, -0.0105071));
    times[11][2] = 1.4;
    keys[11][2] = AL::ALValue::array(1.50021, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[11][3] = 1.88;
    keys[11][3] = AL::ALValue::array(1.52322, AL::ALValue::array(3, -0.16, -0.00840861), AL::ALValue::array(3, 0.2, 0.0105108));
    times[11][4] = 2.48;
    keys[11][4] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.2, -0.00790243), AL::ALValue::array(3, 0.24, 0.00948291));
    times[11][5] = 3.2;
    keys[11][5] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(6);
    keys[12].arraySetSize(6);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.24, -0.00682889), AL::ALValue::array(3, 0.173333, 0.00493198));
    times[12][2] = 1.4;
    keys[12][2] = AL::ALValue::array(0.25, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[12][3] = 1.88;
    keys[12][3] = AL::ALValue::array(0.25, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[12][4] = 2.48;
    keys[12][4] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.2, 0.00534585), AL::ALValue::array(3, 0.24, -0.00641502));
    times[12][5] = 3.2;
    keys[12][5] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(6);
    keys[13].arraySetSize(6);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, -0.00460191), AL::ALValue::array(3, 0.173333, 0.0033236));
    times[13][2] = 1.4;
    keys[13][2] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[13][3] = 1.88;
    keys[13][3] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[13][4] = 2.48;
    keys[13][4] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.2, 0.00383493), AL::ALValue::array(3, 0.24, -0.00460191));
    times[13][5] = 3.2;
    keys[13][5] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(5);
    keys[14].arraySetSize(5);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 0.00118762), AL::ALValue::array(3, 0.173333, -0.000857728));
    times[14][2] = 1.4;
    keys[14][2] = AL::ALValue::array(-0.355846, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[14][3] = 2.48;
    keys[14][3] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.36, -0.00122721), AL::ALValue::array(3, 0.24, 0.000818141));
    times[14][4] = 3.2;
    keys[14][4] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(5);
    keys[15].arraySetSize(5);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[15][2] = 1.4;
    keys[15][2] = AL::ALValue::array(4.19617e-05, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[15][3] = 2.48;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][4] = 3.2;
    keys[15][4] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(6);
    keys[16].arraySetSize(6);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.24, -0.0151421), AL::ALValue::array(3, 0.173333, 0.0109359));
    times[16][2] = 1.4;
    keys[16][2] = AL::ALValue::array(1.39445, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[16][3] = 1.88;
    keys[16][3] = AL::ALValue::array(1.33002, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[16][4] = 2.48;
    keys[16][4] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][5] = 3.2;
    keys[16][5] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(6);
    keys[17].arraySetSize(6);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[17][2] = 1.4;
    keys[17][2] = AL::ALValue::array(1.33607, AL::ALValue::array(3, -0.173333, -0.0640803), AL::ALValue::array(3, 0.16, 0.059151));
    times[17][3] = 1.88;
    keys[17][3] = AL::ALValue::array(1.4818, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[17][4] = 2.48;
    keys[17][4] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][5] = 3.2;
    keys[17][5] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(6);
    keys[18].arraySetSize(6);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[18][2] = 1.4;
    keys[18][2] = AL::ALValue::array(0.1904, AL::ALValue::array(3, -0.173333, -0.00429867), AL::ALValue::array(3, 0.16, 0.003968));
    times[18][3] = 1.88;
    keys[18][3] = AL::ALValue::array(0.202, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[18][4] = 2.48;
    keys[18][4] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][5] = 3.2;
    keys[18][5] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(5);
    keys[19].arraySetSize(5);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 0.00424739), AL::ALValue::array(3, 0.173333, -0.00306756));
    times[19][2] = 1.4;
    keys[19][2] = AL::ALValue::array(-0.713352, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[19][3] = 2.48;
    keys[19][3] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.36, -0.00306756), AL::ALValue::array(3, 0.24, 0.00204504));
    times[19][4] = 3.2;
    keys[19][4] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(5);
    keys[20].arraySetSize(5);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[20][2] = 1.4;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[20][3] = 2.48;
    keys[20][3] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][4] = 3.2;
    keys[20][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(5);
    keys[21].arraySetSize(5);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[21][2] = 1.4;
    keys[21][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[21][3] = 2.48;
    keys[21][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][4] = 3.2;
    keys[21][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(5);
    keys[22].arraySetSize(5);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[22][2] = 1.4;
    keys[22][2] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.36, 0));
    times[22][3] = 2.48;
    keys[22][3] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.36, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][4] = 3.2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(6);
    keys[23].arraySetSize(6);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[23][2] = 1.4;
    keys[23][2] = AL::ALValue::array(0.859082, AL::ALValue::array(3, -0.173333, -0.0912014), AL::ALValue::array(3, 0.16, 0.0841859));
    times[23][3] = 1.88;
    keys[23][3] = AL::ALValue::array(1.10606, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.2, 0));
    times[23][4] = 2.48;
    keys[23][4] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][5] = 3.2;
    keys[23][5] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(6);
    keys[24].arraySetSize(6);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[24][2] = 1.4;
    keys[24][2] = AL::ALValue::array(-0.044528, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[24][3] = 1.88;
    keys[24][3] = AL::ALValue::array(0.0475121, AL::ALValue::array(3, -0.16, -0.0397704), AL::ALValue::array(3, 0.2, 0.049713));
    times[24][4] = 2.48;
    keys[24][4] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.2, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][5] = 3.2;
    keys[24][5] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(6);
    keys[25].arraySetSize(6);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(0.338594, AL::ALValue::array(3, -0.24, -0.188237), AL::ALValue::array(3, 0.173333, 0.135949));
    times[25][2] = 1.4;
    keys[25][2] = AL::ALValue::array(1.03673, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.16, 0));
    times[25][3] = 1.88;
    keys[25][3] = AL::ALValue::array(0.63879, AL::ALValue::array(3, -0.16, 0.122265), AL::ALValue::array(3, 0.2, -0.152832));
    times[25][4] = 2.48;
    keys[25][4] = AL::ALValue::array(0.211651, AL::ALValue::array(3, -0.2, 0.0878564), AL::ALValue::array(3, 0.24, -0.105428));
    times[25][5] = 3.2;
    keys[25][5] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 3
 */
void Draw::motionDraw3() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(7);
    keys[0].arraySetSize(7);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][1] = 0.88;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[0][2] = 1.32;
    keys[0][2] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[0][3] = 1.68;
    keys[0][3] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[0][4] = 2;
    keys[0][4] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[0][5] = 2.48;
    keys[0][5] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][6] = 3.2;
    keys[0][6] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(7);
    keys[1].arraySetSize(7);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][1] = 0.88;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[1][2] = 1.32;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[1][3] = 1.68;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[1][4] = 2;
    keys[1][4] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[1][5] = 2.48;
    keys[1][5] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][6] = 3.2;
    keys[1][6] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(7);
    keys[2].arraySetSize(7);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 0.00349114), AL::ALValue::array(3, 0.146667, -0.00213347));
    times[2][2] = 1.32;
    keys[2][2] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[2][3] = 1.68;
    keys[2][3] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[2][4] = 2;
    keys[2][4] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[2][5] = 2.48;
    keys[2][5] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.16, -0.00224984), AL::ALValue::array(3, 0.24, 0.00337476));
    times[2][6] = 3.2;
    keys[2][6] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(7);
    keys[3].arraySetSize(7);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[3][2] = 1.32;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[3][3] = 1.68;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[3][4] = 2;
    keys[3][4] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[3][5] = 2.48;
    keys[3][5] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][6] = 3.2;
    keys[3][6] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(7);
    keys[4].arraySetSize(7);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.24, 0.00190413), AL::ALValue::array(3, 0.146667, -0.00116364));
    times[4][2] = 1.32;
    keys[4][2] = AL::ALValue::array(-0.184038, AL::ALValue::array(3, -0.146667, 0.00153431), AL::ALValue::array(3, 0.12, -0.00125535));
    times[4][3] = 1.68;
    keys[4][3] = AL::ALValue::array(-0.191709, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[4][4] = 2;
    keys[4][4] = AL::ALValue::array(-0.191709, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[4][5] = 2.48;
    keys[4][5] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.16, -0.00224985), AL::ALValue::array(3, 0.24, 0.00337477));
    times[4][6] = 3.2;
    keys[4][6] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(7);
    keys[5].arraySetSize(7);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, -2.17895e-07), AL::ALValue::array(3, 0.146667, 1.33158e-07));
    times[5][2] = 1.32;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[5][3] = 1.68;
    keys[5][3] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[5][4] = 2;
    keys[5][4] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[5][5] = 2.48;
    keys[5][5] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][6] = 3.2;
    keys[5][6] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(7);
    keys[6].arraySetSize(7);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[6][2] = 1.32;
    keys[6][2] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.146667, 0.000733335), AL::ALValue::array(3, 0.12, -0.000600001));
    times[6][3] = 1.68;
    keys[6][3] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[6][4] = 2;
    keys[6][4] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[6][5] = 2.48;
    keys[6][5] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][6] = 3.2;
    keys[6][6] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(7);
    keys[7].arraySetSize(7);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.24, 0.00251048), AL::ALValue::array(3, 0.146667, -0.00153418));
    times[7][2] = 1.32;
    keys[7][2] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[7][3] = 1.68;
    keys[7][3] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[7][4] = 2;
    keys[7][4] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[7][5] = 2.48;
    keys[7][5] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.16, -0.00153418), AL::ALValue::array(3, 0.24, 0.00230127));
    times[7][6] = 3.2;
    keys[7][6] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(7);
    keys[8].arraySetSize(7);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[8][2] = 1.32;
    keys[8][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[8][3] = 1.68;
    keys[8][3] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[8][4] = 2;
    keys[8][4] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[8][5] = 2.48;
    keys[8][5] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][6] = 3.2;
    keys[8][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(7);
    keys[9].arraySetSize(7);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[9][2] = 1.32;
    keys[9][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[9][3] = 1.68;
    keys[9][3] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[9][4] = 2;
    keys[9][4] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[9][5] = 2.48;
    keys[9][5] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][6] = 3.2;
    keys[9][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(7);
    keys[10].arraySetSize(7);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.24, 0.00251037), AL::ALValue::array(3, 0.146667, -0.00153411));
    times[10][2] = 1.32;
    keys[10][2] = AL::ALValue::array(0.694859, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[10][3] = 1.68;
    keys[10][3] = AL::ALValue::array(0.694859, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[10][4] = 2;
    keys[10][4] = AL::ALValue::array(0.694859, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[10][5] = 2.48;
    keys[10][5] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.16, -0.00153411), AL::ALValue::array(3, 0.24, 0.00230117));
    times[10][6] = 3.2;
    keys[10][6] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(7);
    keys[11].arraySetSize(7);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.24, 0.0184079), AL::ALValue::array(3, 0.146667, -0.0112493));
    times[11][2] = 1.32;
    keys[11][2] = AL::ALValue::array(1.47106, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[11][3] = 1.68;
    keys[11][3] = AL::ALValue::array(1.47567, AL::ALValue::array(3, -0.12, -0.00297784), AL::ALValue::array(3, 0.106667, 0.00264697));
    times[11][4] = 2;
    keys[11][4] = AL::ALValue::array(1.48794, AL::ALValue::array(3, -0.106667, -0.0108404), AL::ALValue::array(3, 0.16, 0.0162605));
    times[11][5] = 2.48;
    keys[11][5] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.16, -0.0116583), AL::ALValue::array(3, 0.24, 0.0174875));
    times[11][6] = 3.2;
    keys[11][6] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(7);
    keys[12].arraySetSize(7);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.24, -0.00793461), AL::ALValue::array(3, 0.146667, 0.00484893));
    times[12][2] = 1.32;
    keys[12][2] = AL::ALValue::array(0.253068, AL::ALValue::array(3, -0.146667, -0.00365606), AL::ALValue::array(3, 0.12, 0.00299132));
    times[12][3] = 1.68;
    keys[12][3] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[12][4] = 2;
    keys[12][4] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[12][5] = 2.48;
    keys[12][5] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.16, 0.00552246), AL::ALValue::array(3, 0.24, -0.00828368));
    times[12][6] = 3.2;
    keys[12][6] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(7);
    keys[13].arraySetSize(7);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, -0.00460191), AL::ALValue::array(3, 0.146667, 0.00281228));
    times[13][2] = 1.32;
    keys[13][2] = AL::ALValue::array(0.0889301, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[13][3] = 1.68;
    keys[13][3] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[13][4] = 2;
    keys[13][4] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[13][5] = 2.48;
    keys[13][5] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.16, 0.00306794), AL::ALValue::array(3, 0.24, -0.00460191));
    times[13][6] = 3.2;
    keys[13][6] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(7);
    keys[14].arraySetSize(7);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 0.00153415), AL::ALValue::array(3, 0.146667, -0.000937534));
    times[14][2] = 1.32;
    keys[14][2] = AL::ALValue::array(-0.357381, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[14][3] = 1.68;
    keys[14][3] = AL::ALValue::array(-0.357381, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[14][4] = 2;
    keys[14][4] = AL::ALValue::array(-0.357381, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[14][5] = 2.48;
    keys[14][5] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.16, -0.00102276), AL::ALValue::array(3, 0.24, 0.00153415));
    times[14][6] = 3.2;
    keys[14][6] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(7);
    keys[15].arraySetSize(7);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[15][2] = 1.32;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[15][3] = 1.68;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[15][4] = 2;
    keys[15][4] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[15][5] = 2.48;
    keys[15][5] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][6] = 3.2;
    keys[15][6] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(7);
    keys[16].arraySetSize(7);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[16][2] = 1.32;
    keys[16][2] = AL::ALValue::array(1.29627, AL::ALValue::array(3, -0.146667, 0.00187519), AL::ALValue::array(3, 0.12, -0.00153425));
    times[16][3] = 1.68;
    keys[16][3] = AL::ALValue::array(1.29474, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[16][4] = 2;
    keys[16][4] = AL::ALValue::array(1.45121, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[16][5] = 2.48;
    keys[16][5] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.16, 0.0182036), AL::ALValue::array(3, 0.24, -0.0273053));
    times[16][6] = 3.2;
    keys[16][6] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(7);
    keys[17].arraySetSize(7);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[17][2] = 1.32;
    keys[17][2] = AL::ALValue::array(1.15659, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[17][3] = 1.68;
    keys[17][3] = AL::ALValue::array(1.14432, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[17][4] = 2;
    keys[17][4] = AL::ALValue::array(1.21489, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[17][5] = 2.48;
    keys[17][5] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][6] = 3.2;
    keys[17][6] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(7);
    keys[18].arraySetSize(7);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.13, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[18][2] = 1.32;
    keys[18][2] = AL::ALValue::array(0.2032, AL::ALValue::array(3, -0.146667, -0.00342224), AL::ALValue::array(3, 0.12, 0.00280002));
    times[18][3] = 1.68;
    keys[18][3] = AL::ALValue::array(0.206, AL::ALValue::array(3, -0.12, -0.000917649), AL::ALValue::array(3, 0.106667, 0.000815688));
    times[18][4] = 2;
    keys[18][4] = AL::ALValue::array(0.2084, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[18][5] = 2.48;
    keys[18][5] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][6] = 3.2;
    keys[18][6] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(7);
    keys[19].arraySetSize(7);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 0.00251037), AL::ALValue::array(3, 0.146667, -0.00153411));
    times[19][2] = 1.32;
    keys[19][2] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[19][3] = 1.68;
    keys[19][3] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[19][4] = 2;
    keys[19][4] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[19][5] = 2.48;
    keys[19][5] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.16, -0.00153411), AL::ALValue::array(3, 0.24, 0.00230117));
    times[19][6] = 3.2;
    keys[19][6] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(7);
    keys[20].arraySetSize(7);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[20][2] = 1.32;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[20][3] = 1.68;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[20][4] = 2;
    keys[20][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[20][5] = 2.48;
    keys[20][5] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][6] = 3.2;
    keys[20][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(7);
    keys[21].arraySetSize(7);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[21][2] = 1.32;
    keys[21][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[21][3] = 1.68;
    keys[21][3] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[21][4] = 2;
    keys[21][4] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[21][5] = 2.48;
    keys[21][5] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][6] = 3.2;
    keys[21][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(7);
    keys[22].arraySetSize(7);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[22][2] = 1.32;
    keys[22][2] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[22][3] = 1.68;
    keys[22][3] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[22][4] = 2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[22][5] = 2.48;
    keys[22][5] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][6] = 3.2;
    keys[22][6] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(7);
    keys[23].arraySetSize(7);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[23][2] = 1.32;
    keys[23][2] = AL::ALValue::array(1.126, AL::ALValue::array(3, -0.146667, -0.0131247), AL::ALValue::array(3, 0.12, 0.0107384));
    times[23][3] = 1.68;
    keys[23][3] = AL::ALValue::array(1.13674, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[23][4] = 2;
    keys[23][4] = AL::ALValue::array(1.06004, AL::ALValue::array(3, -0.106667, 0.0742456), AL::ALValue::array(3, 0.16, -0.111368));
    times[23][5] = 2.48;
    keys[23][5] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][6] = 3.2;
    keys[23][6] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(7);
    keys[24].arraySetSize(7);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[24][2] = 1.32;
    keys[24][2] = AL::ALValue::array(0.171766, AL::ALValue::array(3, -0.146667, 0.0157491), AL::ALValue::array(3, 0.12, -0.0128856));
    times[24][3] = 1.68;
    keys[24][3] = AL::ALValue::array(0.138018, AL::ALValue::array(3, -0.12, 0.009204), AL::ALValue::array(3, 0.106667, -0.00818134));
    times[24][4] = 2;
    keys[24][4] = AL::ALValue::array(0.11961, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[24][5] = 2.48;
    keys[24][5] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][6] = 3.2;
    keys[24][6] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(7);
    keys[25].arraySetSize(7);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(0.31765, AL::ALValue::array(3, -0.24, -0.164402), AL::ALValue::array(3, 0.146667, 0.100468));
    times[25][2] = 1.32;
    keys[25][2] = AL::ALValue::array(0.858998, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[25][3] = 1.68;
    keys[25][3] = AL::ALValue::array(0.728609, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[25][4] = 2;
    keys[25][4] = AL::ALValue::array(0.898883, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[25][5] = 2.48;
    keys[25][5] = AL::ALValue::array(0.211651, AL::ALValue::array(3, -0.16, 0.102267), AL::ALValue::array(3, 0.24, -0.153401));
    times[25][6] = 3.2;
    keys[25][6] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 4
 */
void Draw::motionDraw4() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(7);
    keys[0].arraySetSize(7);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][1] = 0.88;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[0][2] = 1.32;
    keys[0][2] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[0][3] = 1.68;
    keys[0][3] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[0][4] = 2;
    keys[0][4] = AL::ALValue::array(0.00149202, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[0][5] = 2.48;
    keys[0][5] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][6] = 3.2;
    keys[0][6] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(7);
    keys[1].arraySetSize(7);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][1] = 0.88;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[1][2] = 1.32;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[1][3] = 1.68;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[1][4] = 2;
    keys[1][4] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[1][5] = 2.48;
    keys[1][5] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][6] = 3.2;
    keys[1][6] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(7);
    keys[2].arraySetSize(7);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 0.00349114), AL::ALValue::array(3, 0.146667, -0.00213347));
    times[2][2] = 1.32;
    keys[2][2] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.146667, 0.0014063), AL::ALValue::array(3, 0.12, -0.00115061));
    times[2][3] = 1.68;
    keys[2][3] = AL::ALValue::array(-0.360533, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[2][4] = 2;
    keys[2][4] = AL::ALValue::array(-0.357464, AL::ALValue::array(3, -0.106667, -0.00102276), AL::ALValue::array(3, 0.16, 0.00153414));
    times[2][5] = 2.48;
    keys[2][5] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.16, -0.00224984), AL::ALValue::array(3, 0.24, 0.00337476));
    times[2][6] = 3.2;
    keys[2][6] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(7);
    keys[3].arraySetSize(7);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[3][2] = 1.32;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[3][3] = 1.68;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[3][4] = 2;
    keys[3][4] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[3][5] = 2.48;
    keys[3][5] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][6] = 3.2;
    keys[3][6] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(7);
    keys[4].arraySetSize(7);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[4][2] = 1.32;
    keys[4][2] = AL::ALValue::array(-0.174834, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[4][3] = 1.68;
    keys[4][3] = AL::ALValue::array(-0.193243, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[4][4] = 2;
    keys[4][4] = AL::ALValue::array(-0.191709, AL::ALValue::array(3, -0.106667, -0.00143187), AL::ALValue::array(3, 0.16, 0.0021478));
    times[4][5] = 2.48;
    keys[4][5] = AL::ALValue::array(-0.182504, AL::ALValue::array(3, -0.16, -0.00224985), AL::ALValue::array(3, 0.24, 0.00337477));
    times[4][6] = 3.2;
    keys[4][6] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(7);
    keys[5].arraySetSize(7);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, -2.17895e-07), AL::ALValue::array(3, 0.146667, 1.33158e-07));
    times[5][2] = 1.32;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[5][3] = 1.68;
    keys[5][3] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[5][4] = 2;
    keys[5][4] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[5][5] = 2.48;
    keys[5][5] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][6] = 3.2;
    keys[5][6] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(7);
    keys[6].arraySetSize(7);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[6][2] = 1.32;
    keys[6][2] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.146667, 0.000733335), AL::ALValue::array(3, 0.12, -0.000600001));
    times[6][3] = 1.68;
    keys[6][3] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[6][4] = 2;
    keys[6][4] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[6][5] = 2.48;
    keys[6][5] = AL::ALValue::array(0.2696, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][6] = 3.2;
    keys[6][6] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(7);
    keys[7].arraySetSize(7);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.24, 0.00251048), AL::ALValue::array(3, 0.146667, -0.00153418));
    times[7][2] = 1.32;
    keys[7][2] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[7][3] = 1.68;
    keys[7][3] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[7][4] = 2;
    keys[7][4] = AL::ALValue::array(-0.707132, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[7][5] = 2.48;
    keys[7][5] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.16, -0.00153418), AL::ALValue::array(3, 0.24, 0.00230127));
    times[7][6] = 3.2;
    keys[7][6] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(7);
    keys[8].arraySetSize(7);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[8][2] = 1.32;
    keys[8][2] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[8][3] = 1.68;
    keys[8][3] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[8][4] = 2;
    keys[8][4] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[8][5] = 2.48;
    keys[8][5] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][6] = 3.2;
    keys[8][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(7);
    keys[9].arraySetSize(7);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[9][2] = 1.32;
    keys[9][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[9][3] = 1.68;
    keys[9][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[9][4] = 2;
    keys[9][4] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[9][5] = 2.48;
    keys[9][5] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][6] = 3.2;
    keys[9][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(7);
    keys[10].arraySetSize(7);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.24, 0.00251037), AL::ALValue::array(3, 0.146667, -0.00153411));
    times[10][2] = 1.32;
    keys[10][2] = AL::ALValue::array(0.694859, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[10][3] = 1.68;
    keys[10][3] = AL::ALValue::array(0.697927, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[10][4] = 2;
    keys[10][4] = AL::ALValue::array(0.694859, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[10][5] = 2.48;
    keys[10][5] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.16, -0.00153411), AL::ALValue::array(3, 0.24, 0.00230117));
    times[10][6] = 3.2;
    keys[10][6] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(7);
    keys[11].arraySetSize(7);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.24, 0.0184079), AL::ALValue::array(3, 0.146667, -0.0112493));
    times[11][2] = 1.32;
    keys[11][2] = AL::ALValue::array(1.46493, AL::ALValue::array(3, -0.146667, 0.00375006), AL::ALValue::array(3, 0.12, -0.00306823));
    times[11][3] = 1.68;
    keys[11][3] = AL::ALValue::array(1.46186, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[11][4] = 2;
    keys[11][4] = AL::ALValue::array(1.48794, AL::ALValue::array(3, -0.106667, -0.0126811), AL::ALValue::array(3, 0.16, 0.0190217));
    times[11][5] = 2.48;
    keys[11][5] = AL::ALValue::array(1.55697, AL::ALValue::array(3, -0.16, -0.0116583), AL::ALValue::array(3, 0.24, 0.0174875));
    times[11][6] = 3.2;
    keys[11][6] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(7);
    keys[12].arraySetSize(7);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.24, -0.010791), AL::ALValue::array(3, 0.146667, 0.00659452));
    times[12][2] = 1.32;
    keys[12][2] = AL::ALValue::array(0.266874, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[12][3] = 1.68;
    keys[12][3] = AL::ALValue::array(0.25767, AL::ALValue::array(3, -0.12, 0.00172592), AL::ALValue::array(3, 0.106667, -0.00153415));
    times[12][4] = 2;
    keys[12][4] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.106667, 0.00153415), AL::ALValue::array(3, 0.16, -0.00230122));
    times[12][5] = 2.48;
    keys[12][5] = AL::ALValue::array(0.236194, AL::ALValue::array(3, -0.16, 0.00552246), AL::ALValue::array(3, 0.24, -0.00828368));
    times[12][6] = 3.2;
    keys[12][6] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(7);
    keys[13].arraySetSize(7);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.24, -0.00460191), AL::ALValue::array(3, 0.146667, 0.00281228));
    times[13][2] = 1.32;
    keys[13][2] = AL::ALValue::array(0.0889301, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[13][3] = 1.68;
    keys[13][3] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[13][4] = 2;
    keys[13][4] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[13][5] = 2.48;
    keys[13][5] = AL::ALValue::array(0.0536479, AL::ALValue::array(3, -0.16, 0.00306794), AL::ALValue::array(3, 0.24, -0.00460191));
    times[13][6] = 3.2;
    keys[13][6] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(7);
    keys[14].arraySetSize(7);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 0.00153415), AL::ALValue::array(3, 0.146667, -0.000937534));
    times[14][2] = 1.32;
    keys[14][2] = AL::ALValue::array(-0.357381, AL::ALValue::array(3, -0.146667, 0.00562456), AL::ALValue::array(3, 0.12, -0.00460191));
    times[14][3] = 1.68;
    keys[14][3] = AL::ALValue::array(-0.381923, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[14][4] = 2;
    keys[14][4] = AL::ALValue::array(-0.357381, AL::ALValue::array(3, -0.106667, -0.00409059), AL::ALValue::array(3, 0.16, 0.00613588));
    times[14][5] = 2.48;
    keys[14][5] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.16, -0.00102276), AL::ALValue::array(3, 0.24, 0.00153415));
    times[14][6] = 3.2;
    keys[14][6] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(7);
    keys[15].arraySetSize(7);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[15][2] = 1.32;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[15][3] = 1.68;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[15][4] = 2;
    keys[15][4] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[15][5] = 2.48;
    keys[15][5] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][6] = 3.2;
    keys[15][6] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(7);
    keys[16].arraySetSize(7);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.24, -0.034277), AL::ALValue::array(3, 0.146667, 0.020947));
    times[16][2] = 1.32;
    keys[16][2] = AL::ALValue::array(1.48189, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[16][3] = 1.68;
    keys[16][3] = AL::ALValue::array(1.40825, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[16][4] = 2;
    keys[16][4] = AL::ALValue::array(1.45121, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[16][5] = 2.48;
    keys[16][5] = AL::ALValue::array(1.37604, AL::ALValue::array(3, -0.16, 0.0182036), AL::ALValue::array(3, 0.24, -0.0273053));
    times[16][6] = 3.2;
    keys[16][6] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(7);
    keys[17].arraySetSize(7);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[17][2] = 1.32;
    keys[17][2] = AL::ALValue::array(1.18574, AL::ALValue::array(3, -0.146667, -0.0177178), AL::ALValue::array(3, 0.12, 0.0144964));
    times[17][3] = 1.68;
    keys[17][3] = AL::ALValue::array(1.20875, AL::ALValue::array(3, -0.12, -0.00514328), AL::ALValue::array(3, 0.106667, 0.0045718));
    times[17][4] = 2;
    keys[17][4] = AL::ALValue::array(1.21489, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[17][5] = 2.48;
    keys[17][5] = AL::ALValue::array(1.11211, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][6] = 3.2;
    keys[17][6] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(7);
    keys[18].arraySetSize(7);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.13, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[18][2] = 1.32;
    keys[18][2] = AL::ALValue::array(0.2068, AL::ALValue::array(3, -0.146667, -0.0107556), AL::ALValue::array(3, 0.12, 0.00880001));
    times[18][3] = 1.68;
    keys[18][3] = AL::ALValue::array(0.2156, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[18][4] = 2;
    keys[18][4] = AL::ALValue::array(0.2084, AL::ALValue::array(3, -0.106667, 0.00512), AL::ALValue::array(3, 0.16, -0.00768));
    times[18][5] = 2.48;
    keys[18][5] = AL::ALValue::array(0.1772, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][6] = 3.2;
    keys[18][6] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(7);
    keys[19].arraySetSize(7);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 0.00251037), AL::ALValue::array(3, 0.146667, -0.00153411));
    times[19][2] = 1.32;
    keys[19][2] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.146667, 0.00140598), AL::ALValue::array(3, 0.12, -0.00115035));
    times[19][3] = 1.68;
    keys[19][3] = AL::ALValue::array(-0.717953, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[19][4] = 2;
    keys[19][4] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.106667, -0.00102253), AL::ALValue::array(3, 0.16, 0.00153379));
    times[19][5] = 2.48;
    keys[19][5] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.16, -0.00153411), AL::ALValue::array(3, 0.24, 0.00230117));
    times[19][6] = 3.2;
    keys[19][6] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(7);
    keys[20].arraySetSize(7);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[20][2] = 1.32;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[20][3] = 1.68;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[20][4] = 2;
    keys[20][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[20][5] = 2.48;
    keys[20][5] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][6] = 3.2;
    keys[20][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(7);
    keys[21].arraySetSize(7);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[21][2] = 1.32;
    keys[21][2] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[21][3] = 1.68;
    keys[21][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[21][4] = 2;
    keys[21][4] = AL::ALValue::array(-0.00762796, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[21][5] = 2.48;
    keys[21][5] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][6] = 3.2;
    keys[21][6] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(7);
    keys[22].arraySetSize(7);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[22][2] = 1.32;
    keys[22][2] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[22][3] = 1.68;
    keys[22][3] = AL::ALValue::array(0.699545, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[22][4] = 2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[22][5] = 2.48;
    keys[22][5] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][6] = 3.2;
    keys[22][6] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(7);
    keys[23].arraySetSize(7);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[23][2] = 1.32;
    keys[23][2] = AL::ALValue::array(1.21957, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[23][3] = 1.68;
    keys[23][3] = AL::ALValue::array(1.16435, AL::ALValue::array(3, -0.12, 0.0281534), AL::ALValue::array(3, 0.106667, -0.0250252));
    times[23][4] = 2;
    keys[23][4] = AL::ALValue::array(1.06004, AL::ALValue::array(3, -0.106667, 0.0779271), AL::ALValue::array(3, 0.16, -0.116891));
    times[23][5] = 2.48;
    keys[23][5] = AL::ALValue::array(0.579894, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][6] = 3.2;
    keys[23][6] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(7);
    keys[24].arraySetSize(7);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.146667, 0));
    times[24][2] = 1.32;
    keys[24][2] = AL::ALValue::array(0.0168321, AL::ALValue::array(3, -0.146667, 0.0494971), AL::ALValue::array(3, 0.12, -0.0404976));
    times[24][3] = 1.68;
    keys[24][3] = AL::ALValue::array(-0.046062, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[24][4] = 2;
    keys[24][4] = AL::ALValue::array(0.11961, AL::ALValue::array(3, -0.106667, -0.0359979), AL::ALValue::array(3, 0.16, 0.0539969));
    times[24][5] = 2.48;
    keys[24][5] = AL::ALValue::array(0.223922, AL::ALValue::array(3, -0.16, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][6] = 3.2;
    keys[24][6] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(7);
    keys[25].arraySetSize(7);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(0.31765, AL::ALValue::array(3, -0.24, -0.17202), AL::ALValue::array(3, 0.146667, 0.105123));
    times[25][2] = 1.32;
    keys[25][2] = AL::ALValue::array(0.895814, AL::ALValue::array(3, -0.146667, 0), AL::ALValue::array(3, 0.12, 0));
    times[25][3] = 1.68;
    keys[25][3] = AL::ALValue::array(0.799172, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.106667, 0));
    times[25][4] = 2;
    keys[25][4] = AL::ALValue::array(0.898883, AL::ALValue::array(3, -0.106667, 0), AL::ALValue::array(3, 0.16, 0));
    times[25][5] = 2.48;
    keys[25][5] = AL::ALValue::array(0.211651, AL::ALValue::array(3, -0.16, 0.102267), AL::ALValue::array(3, 0.24, -0.153401));
    times[25][6] = 3.2;
    keys[25][6] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 5
 */
void Draw::motionDraw5() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(5);
    keys[0].arraySetSize(5);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[0][1] = 0.72;
    keys[0][1] = AL::ALValue::array(-0.00924587, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[0][2] = 1.36;
    keys[0][2] = AL::ALValue::array(-0.00924587, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[0][3] = 2.04;
    keys[0][3] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[0][4] = 3.2;
    keys[0][4] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(5);
    keys[1].arraySetSize(5);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[1][1] = 0.72;
    keys[1][1] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[1][2] = 1.36;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[1][3] = 2.04;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.226667, 3.12089e-09), AL::ALValue::array(3, 0.386667, -5.32387e-09));
    times[1][4] = 3.2;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(5);
    keys[2].arraySetSize(5);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[2][1] = 0.72;
    keys[2][1] = AL::ALValue::array(-0.34059, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[2][2] = 1.36;
    keys[2][2] = AL::ALValue::array(-0.34059, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[2][3] = 2.04;
    keys[2][3] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[2][4] = 3.2;
    keys[2][4] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(5);
    keys[3].arraySetSize(5);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[3][1] = 0.72;
    keys[3][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[3][2] = 1.36;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[3][3] = 2.04;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[3][4] = 3.2;
    keys[3][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(5);
    keys[4].arraySetSize(5);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[4][1] = 0.72;
    keys[4][1] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[4][2] = 1.36;
    keys[4][2] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[4][3] = 2.04;
    keys[4][3] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.226667, 3.32895e-08), AL::ALValue::array(3, 0.386667, -5.6788e-08));
    times[4][4] = 3.2;
    keys[4][4] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(5);
    keys[5].arraySetSize(5);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[5][1] = 0.72;
    keys[5][1] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[5][2] = 1.36;
    keys[5][2] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[5][3] = 2.04;
    keys[5][3] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[5][4] = 3.2;
    keys[5][4] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(5);
    keys[6].arraySetSize(5);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[6][1] = 0.72;
    keys[6][1] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[6][2] = 1.36;
    keys[6][2] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[6][3] = 2.04;
    keys[6][3] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.226667, 2.98023e-08), AL::ALValue::array(3, 0.386667, -5.08393e-08));
    times[6][4] = 3.2;
    keys[6][4] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(5);
    keys[7].arraySetSize(5);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[7][1] = 0.72;
    keys[7][1] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[7][2] = 1.36;
    keys[7][2] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[7][3] = 2.04;
    keys[7][3] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[7][4] = 3.2;
    keys[7][4] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(5);
    keys[8].arraySetSize(5);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[8][1] = 0.72;
    keys[8][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[8][2] = 1.36;
    keys[8][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[8][3] = 2.04;
    keys[8][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[8][4] = 3.2;
    keys[8][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(5);
    keys[9].arraySetSize(5);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[9][1] = 0.72;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[9][2] = 1.36;
    keys[9][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[9][3] = 2.04;
    keys[9][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.226667, 9.1026e-10), AL::ALValue::array(3, 0.386667, -1.5528e-09));
    times[9][4] = 3.2;
    keys[9][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(5);
    keys[10].arraySetSize(5);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[10][1] = 0.72;
    keys[10][1] = AL::ALValue::array(0.713268, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[10][2] = 1.36;
    keys[10][2] = AL::ALValue::array(0.713268, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[10][3] = 2.04;
    keys[10][3] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.226667, 8.65527e-07), AL::ALValue::array(3, 0.386667, -1.47649e-06));
    times[10][4] = 3.2;
    keys[10][4] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(5);
    keys[11].arraySetSize(5);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[11][1] = 0.72;
    keys[11][1] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[11][2] = 1.36;
    keys[11][2] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[11][3] = 2.04;
    keys[11][3] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[11][4] = 3.2;
    keys[11][4] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(5);
    keys[12].arraySetSize(5);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[12][1] = 0.72;
    keys[12][1] = AL::ALValue::array(0.246932, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[12][2] = 1.36;
    keys[12][2] = AL::ALValue::array(0.246932, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[12][3] = 2.04;
    keys[12][3] = AL::ALValue::array(0.246933, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[12][4] = 3.2;
    keys[12][4] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(5);
    keys[13].arraySetSize(5);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[13][1] = 0.72;
    keys[13][1] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[13][2] = 1.36;
    keys[13][2] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[13][3] = 2.04;
    keys[13][3] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[13][4] = 3.2;
    keys[13][4] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(5);
    keys[14].arraySetSize(5);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[14][1] = 0.72;
    keys[14][1] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[14][2] = 1.36;
    keys[14][2] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[14][3] = 2.04;
    keys[14][3] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[14][4] = 3.2;
    keys[14][4] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(5);
    keys[15].arraySetSize(5);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[15][1] = 0.72;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[15][2] = 1.36;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[15][3] = 2.04;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[15][4] = 3.2;
    keys[15][4] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(5);
    keys[16].arraySetSize(5);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[16][1] = 0.72;
    keys[16][1] = AL::ALValue::array(1.284, AL::ALValue::array(3, -0.186667, 0.0322139), AL::ALValue::array(3, 0.213333, -0.0368158));
    times[16][2] = 1.36;
    keys[16][2] = AL::ALValue::array(0.971064, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[16][3] = 2.04;
    keys[16][3] = AL::ALValue::array(1.284, AL::ALValue::array(3, -0.226667, -0.0179844), AL::ALValue::array(3, 0.386667, 0.0306793));
    times[16][4] = 3.2;
    keys[16][4] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(5);
    keys[17].arraySetSize(5);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[17][1] = 0.72;
    keys[17][1] = AL::ALValue::array(1.64594, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[17][2] = 1.36;
    keys[17][2] = AL::ALValue::array(1.73184, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[17][3] = 2.04;
    keys[17][3] = AL::ALValue::array(1.64594, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[17][4] = 3.2;
    keys[17][4] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(5);
    keys[18].arraySetSize(5);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[18][1] = 0.72;
    keys[18][1] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[18][2] = 1.36;
    keys[18][2] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[18][3] = 2.04;
    keys[18][3] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.226667, -2.98023e-08), AL::ALValue::array(3, 0.386667, 5.08393e-08));
    times[18][4] = 3.2;
    keys[18][4] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(5);
    keys[19].arraySetSize(5);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[19][1] = 0.72;
    keys[19][1] = AL::ALValue::array(-0.452572, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[19][2] = 1.36;
    keys[19][2] = AL::ALValue::array(-0.452572, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[19][3] = 2.04;
    keys[19][3] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.226667, 7.32369e-07), AL::ALValue::array(3, 0.386667, -1.24934e-06));
    times[19][4] = 3.2;
    keys[19][4] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(5);
    keys[20].arraySetSize(5);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[20][1] = 0.72;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[20][2] = 1.36;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[20][3] = 2.04;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.226667, -7.28208e-09), AL::ALValue::array(3, 0.386667, 1.24224e-08));
    times[20][4] = 3.2;
    keys[20][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(5);
    keys[21].arraySetSize(5);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[21][1] = 0.72;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[21][2] = 1.36;
    keys[21][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[21][3] = 2.04;
    keys[21][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.226667, 9.1026e-10), AL::ALValue::array(3, 0.386667, -1.5528e-09));
    times[21][4] = 3.2;
    keys[21][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(5);
    keys[22].arraySetSize(5);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[22][1] = 0.72;
    keys[22][1] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[22][2] = 1.36;
    keys[22][2] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[22][3] = 2.04;
    keys[22][3] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.226667, 6.6579e-08), AL::ALValue::array(3, 0.386667, -1.13576e-07));
    times[22][4] = 3.2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(5);
    keys[23].arraySetSize(5);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[23][1] = 0.72;
    keys[23][1] = AL::ALValue::array(1.21957, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[23][2] = 1.36;
    keys[23][2] = AL::ALValue::array(1.15821, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[23][3] = 2.04;
    keys[23][3] = AL::ALValue::array(1.21957, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[23][4] = 3.2;
    keys[23][4] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(5);
    keys[24].arraySetSize(5);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[24][1] = 0.72;
    keys[24][1] = AL::ALValue::array(0.25, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0.213333, 0));
    times[24][2] = 1.36;
    keys[24][2] = AL::ALValue::array(0.164096, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[24][3] = 2.04;
    keys[24][3] = AL::ALValue::array(0.249999, AL::ALValue::array(3, -0.226667, 0), AL::ALValue::array(3, 0.386667, 0));
    times[24][4] = 3.2;
    keys[24][4] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(5);
    keys[25].arraySetSize(5);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[25][1] = 0.72;
    keys[25][1] = AL::ALValue::array(0.493906, AL::ALValue::array(3, -0.186667, -0.0442943), AL::ALValue::array(3, 0.213333, 0.050622));
    times[25][2] = 1.36;
    keys[25][2] = AL::ALValue::array(0.544528, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.226667, 0));
    times[25][3] = 2.04;
    keys[25][3] = AL::ALValue::array(0.493905, AL::ALValue::array(3, -0.226667, 0.0506225), AL::ALValue::array(3, 0.386667, -0.0863561));
    times[25][4] = 3.2;
    keys[25][4] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.386667, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 6
 */
void Draw::motionDraw6() {
    // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(6);
    keys[0].arraySetSize(6);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][1] = 0.88;
    keys[0][1] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[0][2] = 1.4;
    keys[0][2] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][3] = 2.12;
    keys[0][3] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[0][4] = 2.64;
    keys[0][4] = AL::ALValue::array(-0.0353239, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[0][5] = 3.2;
    keys[0][5] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(6);
    keys[1].arraySetSize(6);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][1] = 0.88;
    keys[1][1] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[1][2] = 1.4;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][3] = 2.12;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[1][4] = 2.64;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[1][5] = 3.2;
    keys[1][5] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(6);
    keys[2].arraySetSize(6);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][1] = 0.88;
    keys[2][1] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[2][2] = 1.4;
    keys[2][2] = AL::ALValue::array(-0.351328, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][3] = 2.12;
    keys[2][3] = AL::ALValue::array(-0.351328, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[2][4] = 2.64;
    keys[2][4] = AL::ALValue::array(-0.352862, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[2][5] = 3.2;
    keys[2][5] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(6);
    keys[3].arraySetSize(6);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][1] = 0.88;
    keys[3][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[3][2] = 1.4;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][3] = 2.12;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[3][4] = 2.64;
    keys[3][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[3][5] = 3.2;
    keys[3][5] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(7);
    keys[4].arraySetSize(7);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][1] = 0.88;
    keys[4][1] = AL::ALValue::array(-0.187106, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[4][2] = 1.4;
    keys[4][2] = AL::ALValue::array(-0.187106, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[4][3] = 1.76;
    keys[4][3] = AL::ALValue::array(-0.210116, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[4][4] = 2.12;
    keys[4][4] = AL::ALValue::array(-0.187106, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[4][5] = 2.64;
    keys[4][5] = AL::ALValue::array(-0.187106, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[4][6] = 3.2;
    keys[4][6] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(7);
    keys[5].arraySetSize(7);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][1] = 0.88;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[5][2] = 1.4;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[5][3] = 1.76;
    keys[5][3] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[5][4] = 2.12;
    keys[5][4] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[5][5] = 2.64;
    keys[5][5] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[5][6] = 3.2;
    keys[5][6] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(7);
    keys[6].arraySetSize(7);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][1] = 0.88;
    keys[6][1] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[6][2] = 1.4;
    keys[6][2] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[6][3] = 1.76;
    keys[6][3] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[6][4] = 2.12;
    keys[6][4] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[6][5] = 2.64;
    keys[6][5] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[6][6] = 3.2;
    keys[6][6] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(6);
    keys[7].arraySetSize(6);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][1] = 0.88;
    keys[7][1] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.24, 0.0106186), AL::ALValue::array(3, 0.173333, -0.00766897));
    times[7][2] = 1.4;
    keys[7][2] = AL::ALValue::array(-0.713267, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][3] = 2.12;
    keys[7][3] = AL::ALValue::array(-0.713267, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[7][4] = 2.64;
    keys[7][4] = AL::ALValue::array(-0.705598, AL::ALValue::array(3, -0.173333, -0.00766897), AL::ALValue::array(3, 0.186667, 0.00825889));
    times[7][5] = 3.2;
    keys[7][5] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(6);
    keys[8].arraySetSize(6);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][1] = 0.88;
    keys[8][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0.0011876), AL::ALValue::array(3, 0.173333, -0.000857711));
    times[8][2] = 1.4;
    keys[8][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][3] = 2.12;
    keys[8][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[8][4] = 2.64;
    keys[8][4] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.173333, -0.000984779), AL::ALValue::array(3, 0.186667, 0.00106053));
    times[8][5] = 3.2;
    keys[8][5] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(6);
    keys[9].arraySetSize(6);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][1] = 0.88;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[9][2] = 1.4;
    keys[9][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][3] = 2.12;
    keys[9][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[9][4] = 2.64;
    keys[9][4] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[9][5] = 3.2;
    keys[9][5] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(6);
    keys[10].arraySetSize(6);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][1] = 0.88;
    keys[10][1] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.24, 0.00445363), AL::ALValue::array(3, 0.173333, -0.00321651));
    times[10][2] = 1.4;
    keys[10][2] = AL::ALValue::array(0.68719, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][3] = 2.12;
    keys[10][3] = AL::ALValue::array(0.68719, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[10][4] = 2.64;
    keys[10][4] = AL::ALValue::array(0.696393, AL::ALValue::array(3, -0.173333, -0.00369303), AL::ALValue::array(3, 0.186667, 0.00397711));
    times[10][5] = 3.2;
    keys[10][5] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(7);
    keys[11].arraySetSize(7);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][1] = 0.88;
    keys[11][1] = AL::ALValue::array(1.53549, AL::ALValue::array(3, -0.24, 0.0163297), AL::ALValue::array(3, 0.173333, -0.0117936));
    times[11][2] = 1.4;
    keys[11][2] = AL::ALValue::array(1.49101, AL::ALValue::array(3, -0.173333, 0.0105753), AL::ALValue::array(3, 0.12, -0.00732136));
    times[11][3] = 1.76;
    keys[11][3] = AL::ALValue::array(1.4818, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[11][4] = 2.12;
    keys[11][4] = AL::ALValue::array(1.49101, AL::ALValue::array(3, -0.12, -0.00732133), AL::ALValue::array(3, 0.173333, 0.0105752));
    times[11][5] = 2.64;
    keys[11][5] = AL::ALValue::array(1.53549, AL::ALValue::array(3, -0.173333, -0.0135407), AL::ALValue::array(3, 0.186667, 0.0145823));
    times[11][6] = 3.2;
    keys[11][6] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(7);
    keys[12].arraySetSize(7);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][1] = 0.88;
    keys[12][1] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[12][2] = 1.4;
    keys[12][2] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[12][3] = 1.76;
    keys[12][3] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[12][4] = 2.12;
    keys[12][4] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[12][5] = 2.64;
    keys[12][5] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[12][6] = 3.2;
    keys[12][6] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(7);
    keys[13].arraySetSize(7);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][1] = 0.88;
    keys[13][1] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[13][2] = 1.4;
    keys[13][2] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[13][3] = 1.76;
    keys[13][3] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[13][4] = 2.12;
    keys[13][4] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[13][5] = 2.64;
    keys[13][5] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[13][6] = 3.2;
    keys[13][6] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(6);
    keys[14].arraySetSize(6);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][1] = 0.88;
    keys[14][1] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.24, 0.00153415), AL::ALValue::array(3, 0.173333, -0.001108));
    times[14][2] = 1.4;
    keys[14][2] = AL::ALValue::array(-0.37272, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][3] = 2.12;
    keys[14][3] = AL::ALValue::array(-0.37272, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[14][4] = 2.64;
    keys[14][4] = AL::ALValue::array(-0.351244, AL::ALValue::array(3, -0.173333, -0.00142457), AL::ALValue::array(3, 0.186667, 0.00153415));
    times[14][5] = 3.2;
    keys[14][5] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(6);
    keys[15].arraySetSize(6);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][1] = 0.88;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[15][2] = 1.4;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][3] = 2.12;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[15][4] = 2.64;
    keys[15][4] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[15][5] = 3.2;
    keys[15][5] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(7);
    keys[16].arraySetSize(7);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][1] = 0.88;
    keys[16][1] = AL::ALValue::array(1.46808, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[16][2] = 1.4;
    keys[16][2] = AL::ALValue::array(1.45274, AL::ALValue::array(3, -0.173333, 0.0153398), AL::ALValue::array(3, 0.12, -0.0106199));
    times[16][3] = 1.76;
    keys[16][3] = AL::ALValue::array(1.30548, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[16][4] = 2.12;
    keys[16][4] = AL::ALValue::array(1.45274, AL::ALValue::array(3, -0.12, -0.0106198), AL::ALValue::array(3, 0.173333, 0.0153397));
    times[16][5] = 2.64;
    keys[16][5] = AL::ALValue::array(1.46808, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[16][6] = 3.2;
    keys[16][6] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(7);
    keys[17].arraySetSize(7);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][1] = 0.88;
    keys[17][1] = AL::ALValue::array(1.04308, AL::ALValue::array(3, -0.24, 0.18883), AL::ALValue::array(3, 0.173333, -0.136377));
    times[17][2] = 1.4;
    keys[17][2] = AL::ALValue::array(0.788434, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[17][3] = 1.76;
    keys[17][3] = AL::ALValue::array(0.921892, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[17][4] = 2.12;
    keys[17][4] = AL::ALValue::array(0.788433, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[17][5] = 2.64;
    keys[17][5] = AL::ALValue::array(1.04308, AL::ALValue::array(3, -0.173333, -0.157568), AL::ALValue::array(3, 0.186667, 0.169688));
    times[17][6] = 3.2;
    keys[17][6] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(7);
    keys[18].arraySetSize(7);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][1] = 0.88;
    keys[18][1] = AL::ALValue::array(0.2008, AL::ALValue::array(3, -0.24, 0.013471), AL::ALValue::array(3, 0.173333, -0.00972903));
    times[18][2] = 1.4;
    keys[18][2] = AL::ALValue::array(0.1844, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.12, 0));
    times[18][3] = 1.76;
    keys[18][3] = AL::ALValue::array(0.198, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[18][4] = 2.12;
    keys[18][4] = AL::ALValue::array(0.1844, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.173333, 0));
    times[18][5] = 2.64;
    keys[18][5] = AL::ALValue::array(0.2008, AL::ALValue::array(3, -0.173333, -0.0127753), AL::ALValue::array(3, 0.186667, 0.013758));
    times[18][6] = 3.2;
    keys[18][6] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(6);
    keys[19].arraySetSize(6);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][1] = 0.88;
    keys[19][1] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.24, 0.00212416), AL::ALValue::array(3, 0.173333, -0.00153411));
    times[19][2] = 1.4;
    keys[19][2] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][3] = 2.12;
    keys[19][3] = AL::ALValue::array(-0.711819, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[19][4] = 2.64;
    keys[19][4] = AL::ALValue::array(-0.710284, AL::ALValue::array(3, -0.173333, -0.00153411), AL::ALValue::array(3, 0.186667, 0.00165212));
    times[19][5] = 3.2;
    keys[19][5] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(6);
    keys[20].arraySetSize(6);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][1] = 0.88;
    keys[20][1] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[20][2] = 1.4;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][3] = 2.12;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[20][4] = 2.64;
    keys[20][4] = AL::ALValue::array(-0.00609397, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[20][5] = 3.2;
    keys[20][5] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(6);
    keys[21].arraySetSize(6);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][1] = 0.88;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[21][2] = 1.4;
    keys[21][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][3] = 2.12;
    keys[21][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[21][4] = 2.64;
    keys[21][4] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[21][5] = 3.2;
    keys[21][5] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(6);
    keys[22].arraySetSize(6);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][1] = 0.88;
    keys[22][1] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.24, 0.00148432), AL::ALValue::array(3, 0.173333, -0.00107201));
    times[22][2] = 1.4;
    keys[22][2] = AL::ALValue::array(0.694945, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][3] = 2.12;
    keys[22][3] = AL::ALValue::array(0.694945, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[22][4] = 2.64;
    keys[22][4] = AL::ALValue::array(0.70108, AL::ALValue::array(3, -0.173333, -0.00123082), AL::ALValue::array(3, 0.186667, 0.0013255));
    times[22][5] = 3.2;
    keys[22][5] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(7);
    keys[23].arraySetSize(7);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][1] = 0.88;
    keys[23][1] = AL::ALValue::array(1.05543, AL::ALValue::array(3, -0.24, -0.018705), AL::ALValue::array(3, 0.173333, 0.0135092));
    times[23][2] = 1.4;
    keys[23][2] = AL::ALValue::array(1.13213, AL::ALValue::array(3, -0.173333, -0.008863), AL::ALValue::array(3, 0.12, 0.00613592));
    times[23][3] = 1.76;
    keys[23][3] = AL::ALValue::array(1.13827, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[23][4] = 2.12;
    keys[23][4] = AL::ALValue::array(1.13213, AL::ALValue::array(3, -0.12, 0.00613592), AL::ALValue::array(3, 0.173333, -0.008863));
    times[23][5] = 2.64;
    keys[23][5] = AL::ALValue::array(1.05543, AL::ALValue::array(3, -0.173333, 0.0157567), AL::ALValue::array(3, 0.186667, -0.0169688));
    times[23][6] = 3.2;
    keys[23][6] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(7);
    keys[24].arraySetSize(7);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][1] = 0.88;
    keys[24][1] = AL::ALValue::array(0.115008, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[24][2] = 1.4;
    keys[24][2] = AL::ALValue::array(0.076658, AL::ALValue::array(3, -0.173333, 0.0102731), AL::ALValue::array(3, 0.12, -0.00711218));
    times[24][3] = 1.76;
    keys[24][3] = AL::ALValue::array(0.0628521, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[24][4] = 2.12;
    keys[24][4] = AL::ALValue::array(0.076658, AL::ALValue::array(3, -0.12, -0.00711218), AL::ALValue::array(3, 0.173333, 0.0102731));
    times[24][5] = 2.64;
    keys[24][5] = AL::ALValue::array(0.115008, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[24][6] = 3.2;
    keys[24][6] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(7);
    keys[25].arraySetSize(7);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][1] = 0.88;
    keys[25][1] = AL::ALValue::array(1.11364, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.173333, 0));
    times[25][2] = 1.4;
    keys[25][2] = AL::ALValue::array(0.819114, AL::ALValue::array(3, -0.173333, 0.0673798), AL::ALValue::array(3, 0.12, -0.0466476));
    times[25][3] = 1.76;
    keys[25][3] = AL::ALValue::array(0.77156, AL::ALValue::array(3, -0.12, 0), AL::ALValue::array(3, 0.12, 0));
    times[25][4] = 2.12;
    keys[25][4] = AL::ALValue::array(0.819114, AL::ALValue::array(3, -0.12, -0.0466476), AL::ALValue::array(3, 0.173333, 0.0673798));
    times[25][5] = 2.64;
    keys[25][5] = AL::ALValue::array(1.11364, AL::ALValue::array(3, -0.173333, 0), AL::ALValue::array(3, 0.186667, 0));
    times[25][6] = 3.2;
    keys[25][6] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.186667, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 7
 */
void Draw::motionDraw7() {
        // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(5);
    keys[0].arraySetSize(5);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[0][1] = 0.8;
    keys[0][1] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[0][2] = 1.56;
    keys[0][2] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][3] = 2.28;
    keys[0][3] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[0][4] = 3.2;
    keys[0][4] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(5);
    keys[1].arraySetSize(5);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[1][1] = 0.8;
    keys[1][1] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[1][2] = 1.56;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][3] = 2.28;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[1][4] = 3.2;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(5);
    keys[2].arraySetSize(5);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[2][1] = 0.8;
    keys[2][1] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[2][2] = 1.56;
    keys[2][2] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][3] = 2.28;
    keys[2][3] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[2][4] = 3.2;
    keys[2][4] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(5);
    keys[3].arraySetSize(5);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[3][1] = 0.8;
    keys[3][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[3][2] = 1.56;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][3] = 2.28;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[3][4] = 3.2;
    keys[3][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(5);
    keys[4].arraySetSize(5);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[4][1] = 0.8;
    keys[4][1] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[4][2] = 1.56;
    keys[4][2] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][3] = 2.28;
    keys[4][3] = AL::ALValue::array(-0.170232, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[4][4] = 3.2;
    keys[4][4] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(5);
    keys[5].arraySetSize(5);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[5][1] = 0.8;
    keys[5][1] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[5][2] = 1.56;
    keys[5][2] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][3] = 2.28;
    keys[5][3] = AL::ALValue::array(-1.53097, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[5][4] = 3.2;
    keys[5][4] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(5);
    keys[6].arraySetSize(5);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[6][1] = 0.8;
    keys[6][1] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[6][2] = 1.56;
    keys[6][2] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][3] = 2.28;
    keys[6][3] = AL::ALValue::array(0.2656, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[6][4] = 3.2;
    keys[6][4] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(5);
    keys[7].arraySetSize(5);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[7][1] = 0.8;
    keys[7][1] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[7][2] = 1.56;
    keys[7][2] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][3] = 2.28;
    keys[7][3] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[7][4] = 3.2;
    keys[7][4] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(5);
    keys[8].arraySetSize(5);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[8][1] = 0.8;
    keys[8][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[8][2] = 1.56;
    keys[8][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][3] = 2.28;
    keys[8][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[8][4] = 3.2;
    keys[8][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(5);
    keys[9].arraySetSize(5);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[9][1] = 0.8;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[9][2] = 1.56;
    keys[9][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[9][3] = 2.28;
    keys[9][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[9][4] = 3.2;
    keys[9][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(5);
    keys[10].arraySetSize(5);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[10][1] = 0.8;
    keys[10][1] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[10][2] = 1.56;
    keys[10][2] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[10][3] = 2.28;
    keys[10][3] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[10][4] = 3.2;
    keys[10][4] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(5);
    keys[11].arraySetSize(5);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[11][1] = 0.8;
    keys[11][1] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[11][2] = 1.56;
    keys[11][2] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][3] = 2.28;
    keys[11][3] = AL::ALValue::array(1.56771, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[11][4] = 3.2;
    keys[11][4] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(5);
    keys[12].arraySetSize(5);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[12][1] = 0.8;
    keys[12][1] = AL::ALValue::array(0.246933, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[12][2] = 1.56;
    keys[12][2] = AL::ALValue::array(0.246933, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][3] = 2.28;
    keys[12][3] = AL::ALValue::array(0.246933, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[12][4] = 3.2;
    keys[12][4] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(5);
    keys[13].arraySetSize(5);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[13][1] = 0.8;
    keys[13][1] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[13][2] = 1.56;
    keys[13][2] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][3] = 2.28;
    keys[13][3] = AL::ALValue::array(0.0858622, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[13][4] = 3.2;
    keys[13][4] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(5);
    keys[14].arraySetSize(5);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[14][1] = 0.8;
    keys[14][1] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[14][2] = 1.56;
    keys[14][2] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][3] = 2.28;
    keys[14][3] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[14][4] = 3.2;
    keys[14][4] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(5);
    keys[15].arraySetSize(5);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[15][1] = 0.8;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[15][2] = 1.56;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][3] = 2.28;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[15][4] = 3.2;
    keys[15][4] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(5);
    keys[16].arraySetSize(5);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[16][1] = 0.8;
    keys[16][1] = AL::ALValue::array(1.00021, AL::ALValue::array(3, -0.213333, 0.0348787), AL::ALValue::array(3, 0.253333, -0.0414184));
    times[16][2] = 1.56;
    keys[16][2] = AL::ALValue::array(0.958791, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[16][3] = 2.28;
    keys[16][3] = AL::ALValue::array(1.00021, AL::ALValue::array(3, -0.24, -0.0414184), AL::ALValue::array(3, 0.306667, 0.0529235));
    times[16][4] = 3.2;
    keys[16][4] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(5);
    keys[17].arraySetSize(5);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[17][1] = 0.8;
    keys[17][1] = AL::ALValue::array(1.017, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[17][2] = 1.56;
    keys[17][2] = AL::ALValue::array(1.29465, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][3] = 2.28;
    keys[17][3] = AL::ALValue::array(1.017, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[17][4] = 3.2;
    keys[17][4] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(5);
    keys[18].arraySetSize(5);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[18][1] = 0.8;
    keys[18][1] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[18][2] = 1.56;
    keys[18][2] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][3] = 2.28;
    keys[18][3] = AL::ALValue::array(0.2448, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[18][4] = 3.2;
    keys[18][4] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(5);
    keys[19].arraySetSize(5);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[19][1] = 0.8;
    keys[19][1] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[19][2] = 1.56;
    keys[19][2] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[19][3] = 2.28;
    keys[19][3] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[19][4] = 3.2;
    keys[19][4] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(5);
    keys[20].arraySetSize(5);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[20][1] = 0.8;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[20][2] = 1.56;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][3] = 2.28;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[20][4] = 3.2;
    keys[20][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(5);
    keys[21].arraySetSize(5);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[21][1] = 0.8;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[21][2] = 1.56;
    keys[21][2] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[21][3] = 2.28;
    keys[21][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[21][4] = 3.2;
    keys[21][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(5);
    keys[22].arraySetSize(5);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[22][1] = 0.8;
    keys[22][1] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[22][2] = 1.56;
    keys[22][2] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[22][3] = 2.28;
    keys[22][3] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[22][4] = 3.2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(5);
    keys[23].arraySetSize(5);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[23][1] = 0.8;
    keys[23][1] = AL::ALValue::array(0.983336, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[23][2] = 1.56;
    keys[23][2] = AL::ALValue::array(1.17202, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[23][3] = 2.28;
    keys[23][3] = AL::ALValue::array(0.983336, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[23][4] = 3.2;
    keys[23][4] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(5);
    keys[24].arraySetSize(5);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[24][1] = 0.8;
    keys[24][1] = AL::ALValue::array(0.243864, AL::ALValue::array(3, -0.213333, -0.00129191), AL::ALValue::array(3, 0.253333, 0.00153415));
    times[24][2] = 1.56;
    keys[24][2] = AL::ALValue::array(0.245399, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][3] = 2.28;
    keys[24][3] = AL::ALValue::array(0.243864, AL::ALValue::array(3, -0.24, 0.00153415), AL::ALValue::array(3, 0.306667, -0.0019603));
    times[24][4] = 3.2;
    keys[24][4] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(5);
    keys[25].arraySetSize(5);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[25][1] = 0.8;
    keys[25][1] = AL::ALValue::array(0.822183, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.253333, 0));
    times[25][2] = 1.56;
    keys[25][2] = AL::ALValue::array(0.424876, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][3] = 2.28;
    keys[25][3] = AL::ALValue::array(0.822183, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.306667, 0));
    times[25][4] = 3.2;
    keys[25][4] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}

/**
 * @brief Markiert das NicNacNoe-Feld 8
 */
void Draw::motionDraw8() {
        // Choregraphe bezier export in c++.
    // Add #include <alproxies/almotionproxy.h> at the beginning of this file.
    std::vector<std::string> names;
    AL::ALValue times, keys;
    names.reserve(26);
    times.arraySetSize(26);
    keys.arraySetSize(26);

    names.push_back("HeadPitch");
    times[0].arraySetSize(5);
    keys[0].arraySetSize(5);

    times[0][0] = 0.16;
    keys[0][0] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[0][1] = 0.8;
    keys[0][1] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[0][2] = 1.52;
    keys[0][2] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[0][3] = 2.28;
    keys[0][3] = AL::ALValue::array(-0.00924586, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[0][4] = 3.2;
    keys[0][4] = AL::ALValue::array(-0.0138481, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("HeadYaw");
    times[1].arraySetSize(5);
    keys[1].arraySetSize(5);

    times[1][0] = 0.16;
    keys[1][0] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[1][1] = 0.8;
    keys[1][1] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[1][2] = 1.52;
    keys[1][2] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[1][3] = 2.28;
    keys[1][3] = AL::ALValue::array(0.0152981, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[1][4] = 3.2;
    keys[1][4] = AL::ALValue::array(0.0106959, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnklePitch");
    times[2].arraySetSize(5);
    keys[2].arraySetSize(5);

    times[2][0] = 0.16;
    keys[2][0] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[2][1] = 0.8;
    keys[2][1] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[2][2] = 1.52;
    keys[2][2] = AL::ALValue::array(-0.346725, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[2][3] = 2.28;
    keys[2][3] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[2][4] = 3.2;
    keys[2][4] = AL::ALValue::array(-0.340591, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LAnkleRoll");
    times[3].arraySetSize(5);
    keys[3].arraySetSize(5);

    times[3][0] = 0.16;
    keys[3][0] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[3][1] = 0.8;
    keys[3][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[3][2] = 1.52;
    keys[3][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[3][3] = 2.28;
    keys[3][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[3][4] = 3.2;
    keys[3][4] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowRoll");
    times[4].arraySetSize(5);
    keys[4].arraySetSize(5);

    times[4][0] = 0.16;
    keys[4][0] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[4][1] = 0.8;
    keys[4][1] = AL::ALValue::array(-0.18097, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[4][2] = 1.52;
    keys[4][2] = AL::ALValue::array(-0.18097, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[4][3] = 2.28;
    keys[4][3] = AL::ALValue::array(-0.18097, AL::ALValue::array(3, -0.253333, -2.49671e-07), AL::ALValue::array(3, 0.306667, 3.02234e-07));
    times[4][4] = 3.2;
    keys[4][4] = AL::ALValue::array(-0.174835, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LElbowYaw");
    times[5].arraySetSize(5);
    keys[5].arraySetSize(5);

    times[5][0] = 0.16;
    keys[5][0] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[5][1] = 0.8;
    keys[5][1] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[5][2] = 1.52;
    keys[5][2] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[5][3] = 2.28;
    keys[5][3] = AL::ALValue::array(-1.5233, AL::ALValue::array(3, -0.253333, 1.33158e-07), AL::ALValue::array(3, 0.306667, -1.61191e-07));
    times[5][4] = 3.2;
    keys[5][4] = AL::ALValue::array(-1.53404, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHand");
    times[6].arraySetSize(5);
    keys[6].arraySetSize(5);

    times[6][0] = 0.16;
    keys[6][0] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[6][1] = 0.8;
    keys[6][1] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[6][2] = 1.52;
    keys[6][2] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[6][3] = 2.28;
    keys[6][3] = AL::ALValue::array(0.2684, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[6][4] = 3.2;
    keys[6][4] = AL::ALValue::array(0.26, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipPitch");
    times[7].arraySetSize(5);
    keys[7].arraySetSize(5);

    times[7][0] = 0.16;
    keys[7][0] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[7][1] = 0.8;
    keys[7][1] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[7][2] = 1.52;
    keys[7][2] = AL::ALValue::array(-0.454021, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[7][3] = 2.28;
    keys[7][3] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[7][4] = 3.2;
    keys[7][4] = AL::ALValue::array(-0.45709, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipRoll");
    times[8].arraySetSize(5);
    keys[8].arraySetSize(5);

    times[8][0] = 0.16;
    keys[8][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[8][1] = 0.8;
    keys[8][1] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[8][2] = 1.52;
    keys[8][2] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[8][3] = 2.28;
    keys[8][3] = AL::ALValue::array(-0.00916195, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[8][4] = 3.2;
    keys[8][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LHipYawPitch");
    times[9].arraySetSize(5);
    keys[9].arraySetSize(5);

    times[9][0] = 0.16;
    keys[9][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[9][1] = 0.8;
    keys[9][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, -0.000481252), AL::ALValue::array(3, 0.24, 0.000541408));
    times[9][2] = 1.52;
    keys[9][2] = AL::ALValue::array(4.19617e-05, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[9][3] = 2.28;
    keys[9][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.253333, 0.000462632), AL::ALValue::array(3, 0.306667, -0.000560028));
    times[9][4] = 3.2;
    keys[9][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LKneePitch");
    times[10].arraySetSize(5);
    keys[10].arraySetSize(5);

    times[10][0] = 0.16;
    keys[10][0] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[10][1] = 0.8;
    keys[10][1] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.213333, -0.000721675), AL::ALValue::array(3, 0.24, 0.000811884));
    times[10][2] = 1.52;
    keys[10][2] = AL::ALValue::array(0.714801, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[10][3] = 2.28;
    keys[10][3] = AL::ALValue::array(0.713267, AL::ALValue::array(3, -0.253333, 0.000693753), AL::ALValue::array(3, 0.306667, -0.000839806));
    times[10][4] = 3.2;
    keys[10][4] = AL::ALValue::array(0.710201, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderPitch");
    times[11].arraySetSize(5);
    keys[11].arraySetSize(5);

    times[11][0] = 0.16;
    keys[11][0] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[11][1] = 0.8;
    keys[11][1] = AL::ALValue::array(1.55543, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[11][2] = 1.52;
    keys[11][2] = AL::ALValue::array(1.55543, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[11][3] = 2.28;
    keys[11][3] = AL::ALValue::array(1.55543, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[11][4] = 3.2;
    keys[11][4] = AL::ALValue::array(1.57538, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LShoulderRoll");
    times[12].arraySetSize(5);
    keys[12].arraySetSize(5);

    times[12][0] = 0.16;
    keys[12][0] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[12][1] = 0.8;
    keys[12][1] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[12][2] = 1.52;
    keys[12][2] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[12][3] = 2.28;
    keys[12][3] = AL::ALValue::array(0.256136, AL::ALValue::array(3, -0.253333, 1.49803e-07), AL::ALValue::array(3, 0.306667, -1.8134e-07));
    times[12][4] = 3.2;
    keys[12][4] = AL::ALValue::array(0.214717, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("LWristYaw");
    times[13].arraySetSize(5);
    keys[13].arraySetSize(5);

    times[13][0] = 0.16;
    keys[13][0] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[13][1] = 0.8;
    keys[13][1] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[13][2] = 1.52;
    keys[13][2] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[13][3] = 2.28;
    keys[13][3] = AL::ALValue::array(0.075124, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[13][4] = 3.2;
    keys[13][4] = AL::ALValue::array(0.049046, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnklePitch");
    times[14].arraySetSize(5);
    keys[14].arraySetSize(5);

    times[14][0] = 0.16;
    keys[14][0] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[14][1] = 0.8;
    keys[14][1] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[14][2] = 1.52;
    keys[14][2] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[14][3] = 2.28;
    keys[14][3] = AL::ALValue::array(-0.352778, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[14][4] = 3.2;
    keys[14][4] = AL::ALValue::array(-0.34971, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RAnkleRoll");
    times[15].arraySetSize(5);
    keys[15].arraySetSize(5);

    times[15][0] = 0.16;
    keys[15][0] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[15][1] = 0.8;
    keys[15][1] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[15][2] = 1.52;
    keys[15][2] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[15][3] = 2.28;
    keys[15][3] = AL::ALValue::array(0.00771189, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[15][4] = 3.2;
    keys[15][4] = AL::ALValue::array(0.00157595, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowRoll");
    times[16].arraySetSize(5);
    keys[16].arraySetSize(5);

    times[16][0] = 0.16;
    keys[16][0] = AL::ALValue::array(1.31621, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[16][1] = 0.8;
    keys[16][1] = AL::ALValue::array(1.28553, AL::ALValue::array(3, -0.213333, 0.0306799), AL::ALValue::array(3, 0.24, -0.0345149));
    times[16][2] = 1.52;
    keys[16][2] = AL::ALValue::array(1.10912, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[16][3] = 2.28;
    keys[16][3] = AL::ALValue::array(1.28553, AL::ALValue::array(3, -0.253333, -0.0240765), AL::ALValue::array(3, 0.306667, 0.0291452));
    times[16][4] = 3.2;
    keys[16][4] = AL::ALValue::array(1.31468, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RElbowYaw");
    times[17].arraySetSize(5);
    keys[17].arraySetSize(5);

    times[17][0] = 0.16;
    keys[17][0] = AL::ALValue::array(1.76406, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[17][1] = 0.8;
    keys[17][1] = AL::ALValue::array(1.04001, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[17][2] = 1.52;
    keys[17][2] = AL::ALValue::array(1.36829, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[17][3] = 2.28;
    keys[17][3] = AL::ALValue::array(1.04001, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[17][4] = 3.2;
    keys[17][4] = AL::ALValue::array(1.7702, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHand");
    times[18].arraySetSize(5);
    keys[18].arraySetSize(5);

    times[18][0] = 0.16;
    keys[18][0] = AL::ALValue::array(0.254, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[18][1] = 0.8;
    keys[18][1] = AL::ALValue::array(0.2588, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[18][2] = 1.52;
    keys[18][2] = AL::ALValue::array(0.2588, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[18][3] = 2.28;
    keys[18][3] = AL::ALValue::array(0.2588, AL::ALValue::array(3, -0.253333, -2.98023e-08), AL::ALValue::array(3, 0.306667, 3.60765e-08));
    times[18][4] = 3.2;
    keys[18][4] = AL::ALValue::array(0.264, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipPitch");
    times[19].arraySetSize(5);
    keys[19].arraySetSize(5);

    times[19][0] = 0.16;
    keys[19][0] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[19][1] = 0.8;
    keys[19][1] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.213333, -0.000721952), AL::ALValue::array(3, 0.24, 0.000812196));
    times[19][2] = 1.52;
    keys[19][2] = AL::ALValue::array(-0.451038, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[19][3] = 2.28;
    keys[19][3] = AL::ALValue::array(-0.452573, AL::ALValue::array(3, -0.253333, 0.000694019), AL::ALValue::array(3, 0.306667, -0.000840128));
    times[19][4] = 3.2;
    keys[19][4] = AL::ALValue::array(-0.455641, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipRoll");
    times[20].arraySetSize(5);
    keys[20].arraySetSize(5);

    times[20][0] = 0.16;
    keys[20][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[20][1] = 0.8;
    keys[20][1] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[20][2] = 1.52;
    keys[20][2] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[20][3] = 2.28;
    keys[20][3] = AL::ALValue::array(-0.00455999, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[20][4] = 3.2;
    keys[20][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RHipYawPitch");
    times[21].arraySetSize(5);
    keys[21].arraySetSize(5);

    times[21][0] = 0.16;
    keys[21][0] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[21][1] = 0.8;
    keys[21][1] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.213333, -0.000481252), AL::ALValue::array(3, 0.24, 0.000541408));
    times[21][2] = 1.52;
    keys[21][2] = AL::ALValue::array(4.19617e-05, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[21][3] = 2.28;
    keys[21][3] = AL::ALValue::array(-0.00149202, AL::ALValue::array(3, -0.253333, 0.000462632), AL::ALValue::array(3, 0.306667, -0.000560028));
    times[21][4] = 3.2;
    keys[21][4] = AL::ALValue::array(-0.00302602, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RKneePitch");
    times[22].arraySetSize(5);
    keys[22].arraySetSize(5);

    times[22][0] = 0.16;
    keys[22][0] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[22][1] = 0.8;
    keys[22][1] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.213333, -0.000962592), AL::ALValue::array(3, 0.24, 0.00108292));
    times[22][2] = 1.52;
    keys[22][2] = AL::ALValue::array(0.70875, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[22][3] = 2.28;
    keys[22][3] = AL::ALValue::array(0.705682, AL::ALValue::array(3, -0.253333, 0.000925349), AL::ALValue::array(3, 0.306667, -0.00112016));
    times[22][4] = 3.2;
    keys[22][4] = AL::ALValue::array(0.702614, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderPitch");
    times[23].arraySetSize(5);
    keys[23].arraySetSize(5);

    times[23][0] = 0.16;
    keys[23][0] = AL::ALValue::array(1.03549, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[23][1] = 0.8;
    keys[23][1] = AL::ALValue::array(1.12293, AL::ALValue::array(3, -0.213333, -0.0507725), AL::ALValue::array(3, 0.24, 0.057119));
    times[23][2] = 1.52;
    keys[23][2] = AL::ALValue::array(1.35917, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[23][3] = 2.28;
    keys[23][3] = AL::ALValue::array(1.12293, AL::ALValue::array(3, -0.253333, 0.0490394), AL::ALValue::array(3, 0.306667, -0.0593635));
    times[23][4] = 3.2;
    keys[23][4] = AL::ALValue::array(1.03396, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RShoulderRoll");
    times[24].arraySetSize(5);
    keys[24].arraySetSize(5);

    times[24][0] = 0.16;
    keys[24][0] = AL::ALValue::array(-0.0813439, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[24][1] = 0.8;
    keys[24][1] = AL::ALValue::array(-0.121228, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[24][2] = 1.52;
    keys[24][2] = AL::ALValue::array(-0.0798099, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[24][3] = 2.28;
    keys[24][3] = AL::ALValue::array(-0.121228, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[24][4] = 3.2;
    keys[24][4] = AL::ALValue::array(-0.0859461, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    names.push_back("RWristYaw");
    times[25].arraySetSize(5);
    keys[25].arraySetSize(5);

    times[25][0] = 0.16;
    keys[25][0] = AL::ALValue::array(0.0643861, AL::ALValue::array(3, -0.0533333, 0), AL::ALValue::array(3, 0.213333, 0));
    times[25][1] = 0.8;
    keys[25][1] = AL::ALValue::array(1.04461, AL::ALValue::array(3, -0.213333, 0), AL::ALValue::array(3, 0.24, 0));
    times[25][2] = 1.52;
    keys[25][2] = AL::ALValue::array(0.931096, AL::ALValue::array(3, -0.24, 0), AL::ALValue::array(3, 0.253333, 0));
    times[25][3] = 2.28;
    keys[25][3] = AL::ALValue::array(1.04461, AL::ALValue::array(3, -0.253333, 0), AL::ALValue::array(3, 0.306667, 0));
    times[25][4] = 3.2;
    keys[25][4] = AL::ALValue::array(0.05825, AL::ALValue::array(3, -0.306667, 0), AL::ALValue::array(3, 0, 0));

    try
    {
      getParentBroker()->getMotionProxy()->angleInterpolationBezier(names, times, keys);
    }
    catch(const std::exception&)
    {

    }
}
