# NicNacNoe #

### Projekt der TH-Wildau_Telematik_T14 ###

In diesem Projekt sollte der Roboter nao näher kennengelernt werden. Implementiert wurde dazu das Spiel TicTacToe.

### Benutzung des Projektes ###

Das Projekt ist eine Arbeit, welche mit der Software QT-Creator erstellt wurde und compiliert werden kann. Ebenfalls wurde die Software Choreographe verwendet.

 * qibuild create thwild_nicnacnoe
 * git clone https://bitbucket.org/Leubeling/thwild_nicnacnoe.git
 * qibuild configure thwild_nicnacnoe
 * qibuild open thwild_nicnacnoe

### Dokumentation ###

Eine Dokumentation zur Arbeit finden Sie, getrennt nach den Zielgruppen Anwender und Entwickler, als Quelltext für LaTex unter dokumentation.