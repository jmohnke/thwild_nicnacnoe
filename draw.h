#ifndef DRAW_H
#define DRAW_H

#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <alcommon/almodule.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alproxies/alrobotpostureproxy.h>

using namespace AL;
namespace AL {class ALBroker;}

/**
 * @brief Modul, welches den nao mit einem Stift Zeichnen lässt.
 * Erzeugt das Event "PenInHand" sobald eine Änderung an dem Stift erkannt wird.
 * Aktualisiert die Daten mit dem Namen "hand" in true/false.
 */
class Draw : public ALModule {
private:
    ALMemoryProxy memoryProxy;
    ALMotionProxy motionProxy;
    ALRobotPostureProxy postureProxy;
    /**
     * @brief Lässt den Roboter,unabhängig seiner aktuellen Pose, die Position mit dem Namen Init einnehmen.
     */
    void moveToInit();
    /**
     * @brief Öffnet oder schließt die Hand des Roboters.
     */
    void toggleHand();
    /**
     * @brief Bewegungsablauf von Stand in die Position mit dem Namen Init.
     */
    void motionStandToInit();
    /**
     * @brief Bewegungsablauf von der Position Init zur Position um einen Stift in Empfang zu nehmen.
     */
    void motionInitToGrab();
    /**
     * @brief Bewegungsablauf von dem Empfang eines Stiftes zurück zur Position mit dem Namen Init.
     */
    void motionGrabToInit();
    /**
     * @brief Markiert das NicNacNoe-Feld 0
     */
    void motionDraw0();
    /**
     * @brief Markiert das NicNacNoe-Feld 1
     */
    void motionDraw1();
    /**
     * @brief Markiert das NicNacNoe-Feld 2
     */
    void motionDraw2();
    /**
     * @brief Markiert das NicNacNoe-Feld 3
     */
    void motionDraw3();
    /**
     * @brief Markiert das NicNacNoe-Feld 4
     */
    void motionDraw4();
    /**
     * @brief Markiert das NicNacNoe-Feld 5
     */
    void motionDraw5();
    /**
     * @brief Markiert das NicNacNoe-Feld 6
     */
    void motionDraw6();
    /**
     * @brief Markiert das NicNacNoe-Feld 7
     */
    void motionDraw7();
    /**
     * @brief Markiert das NicNacNoe-Feld 8
     */
    void motionDraw8();

public:
    /**
     * @brief Konstruktor des Moduls.
     *
     * @param broker Der verwendete Broker aus der naoqi.
     * @param name Der Name des Moduls, unter welchem es ansprechbar ist.
     */
    Draw(boost::shared_ptr<ALBroker> broker, const std::string &name);
    /**
     * @brief Destruktor des Moduls.
     */
    virtual ~Draw();
    /**
     * @brief Startes das Modul um eine Position zum Zeichnen einzunehmen und einen Stift in Empfang zu nehmen.
     */
    void start();
    /**
     * @brief Beendet das Modul und koppelt sich von allen Events ab.
     */
    void stop();
    /**
     * @brief Setzt eine Markierung in ein NicNacNoe-Feld.
     *
     * @param fieldNumber Die Feldnummer der Markierung.
     */
    void drawNicnacnoeField(int fieldNumber);
};

#endif // DRAW_H
